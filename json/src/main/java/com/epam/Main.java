package com.epam;


import com.epam.model.Certificate;
import com.epam.model.Medicine;
import com.epam.model.Package;
import com.epam.model.Versions;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Main {

  public static void main(String[] args) throws Exception {
    File json = new File("src/main/resources/medicine.json");
    GsonParse parser = new GsonParse();
    print(parser.getMedicine(json), "GSON");

    JacksonParse parser2 = new JacksonParse();
    print(parser2.getMedicines(json), "Jackson");

    JSONSchemeValidator.validate();
    generateJson();

  }

  private static void print(List<Medicine> medicines, String type) {
    System.out.println(type);
    medicines.stream()
        .sorted(Comparator.comparingInt(o -> o.getVersions().getaPackage().getPrice()))
        .forEach(System.out::println);
  }

  private static void generateJson() throws ParseException, IOException {
    Medicine medicine = new Medicine(0, "Nazyferon", null, "antiviral",
        Arrays.asList("anaferon", "mezum"),
        new Versions("pills",
            new Certificate(10, new SimpleDateFormat("yyyy-MM-dd").parse("2017-10-24"),
                new SimpleDateFormat("yyyy-MM-dd").parse("2018-10-24"), "Sinevo"),
            new Package("cardboard", 15, 100), "3 pills per day"));
    GsonBuilder builder = new GsonBuilder();
    builder.serializeNulls();
    Gson gson = builder.create();
    String json = gson.toJson(medicine);
    System.out.println("Generating...");
    System.out.println(json);

    JsonFactory jsonFactory = new JsonFactory();
    JsonGenerator generator = jsonFactory.createGenerator(new File(
            "C:\\projects\\epamHomework\\homeworkkoshelia\\json\\src\\main\\resources\\medicine2.json"),
        JsonEncoding.UTF8);
    generator.writeStartObject();
    generator.writeNumberField("medicineId", medicine.getMedicineId());
    generator.writeStringField("name", medicine.getName());
    generator.writeArrayFieldStart("analogs");
    generator.writeString("nazyferon");
    generator.writeString("anaferon");
    generator.writeEndArray();
    generator.writeObjectFieldStart("package");
    generator.writeStringField("type", medicine.getVersions().getaPackage().getType());
    generator.writeNumberField("count", medicine.getVersions().getaPackage().getCount());
    generator.writeNumberField("price", medicine.getVersions().getaPackage().getPrice());
    generator.writeEndObject();
    generator.writeStringField("group", medicine.getGroup());
    generator.writeEndObject();
    generator.close();
  }
}
