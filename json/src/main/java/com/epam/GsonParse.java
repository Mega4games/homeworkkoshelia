package com.epam;

import com.epam.model.Medicine;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

public class GsonParse {

  public List<Medicine> getMedicine(File jsonFile) throws FileNotFoundException {
    Medicine[] medicines;
    GsonBuilder builder = new GsonBuilder();
    Gson gson = builder.create();
    JsonReader reader = new JsonReader(new FileReader(jsonFile));
    medicines = gson.fromJson(reader, Medicine[].class);
    return Arrays.asList(medicines);
  }
}
