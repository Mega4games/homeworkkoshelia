package com.epam.model;

import java.util.Date;

public class Certificate {
  private int number;
  private Date issueDate;
  private Date finishDate;
  private String organization;

  public Certificate() {
  }

  public Certificate(int number, Date issueDate, Date finishDate, String organization) {
    this.number = number;
    this.issueDate = issueDate;
    this.finishDate = finishDate;
    this.organization = organization;
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  public Date getIssueDate() {
    return issueDate;
  }

  public void setIssueDate(Date issueDate) {
    this.issueDate = issueDate;
  }

  public Date getFinishDate() {
    return finishDate;
  }

  public void setFinishDate(Date finishDate) {
    this.finishDate = finishDate;
  }

  public String getOrganization() {
    return organization;
  }

  public void setOrganization(String organization) {
    this.organization = organization;
  }
}