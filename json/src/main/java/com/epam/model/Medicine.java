package com.epam.model;

import java.util.List;

public class Medicine {

  private int medicineId;
  private String name;
  private String pharm;
  private String group;
  private List<String> analogs;
  private Versions versions;

  public Medicine() {

  }

  public Medicine(int medicineId, String name, String pharm, String group,
      List<String> analogs, Versions versions) {
    this.medicineId = medicineId;
    this.name = name;
    this.pharm = pharm;
    this.group = group;
    this.analogs = analogs;
    this.versions = versions;
  }

  public int getMedicineId() {
    return medicineId;
  }

  public void setMedicineId(int medicineId) {
    this.medicineId = medicineId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPharm() {
    return pharm;
  }

  public void setPharm(String pharm) {
    this.pharm = pharm;
  }

  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  public List<String> getAnalogs() {
    return analogs;
  }

  public void setAnalogs(List<String> analogs) {
    this.analogs = analogs;
  }

  public Versions getVersions() {
    return versions;
  }

  public void setVersions(Versions versions) {
    this.versions = versions;
  }

  @Override
  public String toString() {
    return "Medicine{" +
        "medicineId=" + medicineId +
        ", name='" + name + '\'' +
        ", pharm='" + pharm + '\'' +
        ", group='" + group + '\'' +
        ", analogs=" + analogs +
        ", consistency=" + versions.getConsistency() +
        ", certificate: number=" + versions.getCertificate().getNumber() +
        ", issueDate=" + versions.getCertificate().getIssueDate() +
        ", finishDate=" + versions.getCertificate().getFinishDate() +
        ", organization=" + versions.getCertificate().getOrganization() +
        ", package: type=" + versions.getaPackage().getType() +
        ", count=" + versions.getaPackage().getCount() +
        ", price=" + versions.getaPackage().getPrice() +
        ", dossage=" + versions.getDossage() +
        '}';
  }
}