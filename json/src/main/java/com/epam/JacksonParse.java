package com.epam;

import com.epam.model.Medicine;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonParse {

  public List<Medicine> getMedicines(File jsonFile) {
    ObjectMapper mapper = new ObjectMapper();
    Medicine[] medicines = null;
    try {
      medicines = mapper.readValue(jsonFile, Medicine[].class);
    } catch (IOException e) {
      System.out.println("Can't parse.");
      e.printStackTrace();
    }
    return Arrays.asList(medicines);
  }

}
