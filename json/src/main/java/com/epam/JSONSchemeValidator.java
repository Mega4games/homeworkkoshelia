package com.epam;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import org.apache.commons.io.IOUtils;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.*;
import java.io.InputStream;
import org.json.JSONTokener;


public class JSONSchemeValidator {

  public static void validate() {
    try (InputStream inputStream = JSONSchemeValidator.class
        .getResourceAsStream("/medicineScheme.json");
        InputStream is =
            new FileInputStream(
                "C:\\projects\\epamHomework\\homeworkkoshelia\\json\\src\\main\\resources\\medicine.json")) {
      JSONObject schemaValidation = new JSONObject(new JSONTokener(inputStream));
      Schema schema = SchemaLoader.load(schemaValidation);

      String jsonTxt = IOUtils.toString(is, "UTF-8");
      //System.out.println(jsonTxt);
      schema.validate(new JSONArray(jsonTxt));
    } catch (ValidationException e) {
      e.getCausingExceptions().stream()
          .map(ValidationException::getMessage)
          .forEach(System.out::println);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
