package com.epam;

import org.apache.logging.log4j.*;

public class TestLog {

  private static Logger log = LogManager.getLogger(TestLog.class);

  public static void test() {
    log.error("This is an error message twilio");
    log.fatal("This is a fatal message twilio");
  }

}
