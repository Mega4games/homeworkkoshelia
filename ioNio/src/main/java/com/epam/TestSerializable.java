package com.epam;

import java.io.Serializable;

public class TestSerializable implements Serializable {

  private byte version = 1;
  private String serialization = "Serialization";
  private transient int number = 0;


  public byte getVersion() {
    return version;
  }

  public String getSerialization() {
    return serialization;
  }


}
