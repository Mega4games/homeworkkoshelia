package com.epam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PushbackInputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Main {

  public static void main(String[] args) throws Exception {
    //testSerialization();
    //comparePerformance();
    //testStream();
    //testJavaDoc();
    //testFileManager();
    testBuffer();
  }

  private static void testSerialization() {
    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("test.ser"));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("test.ser"))) {
      TestSerializable ts = new TestSerializable();
      oos.writeObject(ts);
      oos.close();
      TestSerializable ts1 = (TestSerializable) ois.readObject();
      System.out.println(ts1.getVersion());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void comparePerformance() throws IOException {
    File file = new File("C:\\projects\\epamHomework\\Java_8.pdf");
    File file2 = new File("C:\\projects\\epamHomework\\Java_8.txt");
    System.out.println("Size of file pdf: " + (float) file.length() / 1048576 + " MB.");
    System.out.println("Size of file txt: " + (float) file2.length() / 1048576 + " MB.");
    StringBuilder text = new StringBuilder();
    long startTime, endTime;
    try (BufferedReader reader = new BufferedReader(
        new FileReader(file))) {
      String currentLine;
      startTime = System.currentTimeMillis();
      while ((currentLine = reader.readLine()) != null) {
        text.append(currentLine);
      }
      endTime = System.currentTimeMillis();
    }

    System.out.println(
        "Reading from file using BufferedReader takes " + (endTime - startTime) + " milliseconds.");
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file2))) {
      startTime = System.currentTimeMillis();
      writer.write(text.toString());
      endTime = System.currentTimeMillis();
    }
    System.out.println(
        "Writing to file using BufferedWriter takes " + (endTime - startTime) + " milliseconds.");
    System.out.println("Size of file pdf: " + (float) file.length() / 1048576 + " MB.");
    System.out.println("Size of file txt: " + (float) file2.length() / 1048576 + " MB.");
    new FileWriter(file2).close();
    text.delete(0, text.length());
    System.out.println("Size of file pdf: " + (float) file.length() / 1048576 + " MB.");
    System.out.println("Size of file txt: " + (float) file2.length() / 1048576 + " MB.");
    try (FileInputStream fileInputStream = new FileInputStream(file)) {
      int content;
      startTime = System.currentTimeMillis();
      while ((content = fileInputStream.read()) != -1) {
        text.append((char) content);
      }
      endTime = System.currentTimeMillis();
    }
    System.out.println(
        "Reading from file using FileInputStream takes " + (endTime - startTime)
            + " milliseconds.");
    try (FileOutputStream fileOutputStream = new FileOutputStream(file2)) {
      char[] chars = text.toString().toCharArray();
      startTime = System.currentTimeMillis();
      for (int i = 0; i < chars.length; i++) {
        fileOutputStream.write(chars[i]);
      }
      endTime = System.currentTimeMillis();
    }
    System.out.println(
        "Writing to file using FileOutputStream takes " + (endTime - startTime) + " milliseconds.");
    BufferedReader reader;
    for (int i = 10; i < 100; i = i + 10) {
      reader = new BufferedReader(new FileReader(file));
      System.out.println("Buffer size: " + i);
      startTime = System.currentTimeMillis();
      while (reader.readLine() != null) {

      }
      endTime = System.currentTimeMillis();
      System.out.println("It takes: " + (endTime - startTime));
      reader.close();
    }
  }


  private static void testStream() throws IOException {
    File file = new File(
        "C:\\projects\\epamHomework\\homeworkkoshelia\\ioNio\\src\\main\\resources\\stream.txt");

    CustomInputStream inputStream = new CustomInputStream(new FileInputStream(file
    ), 10);
    byte[] b = new byte[(int) file.length()];
    inputStream.readWithPushback(b, 0, b.length - 4);
    inputStream.pushBack(b, 0, 2);
    inputStream.readWithPushback(b, 2, 3);
    for (int i = 0; i < b.length; i++) {
      System.out.print((char) b[i]);
    }
    inputStream.close();
  }

  private static void testJavaDoc() throws IOException {
    ZipFile zipFile = new ZipFile("C:\\Program Files\\Java\\jdk1.8.0_131\\src.zip");
    Enumeration<? extends ZipEntry> entries = zipFile.entries();

    while(entries.hasMoreElements()){
      ZipEntry entry = entries.nextElement();
      System.out.println(entry.getName());
    }
    System.out.println("Enter the path of javadoc: ");
    Scanner scanner = new Scanner(System.in);
    String name = scanner.next();
    InputStream fileInputStream = zipFile
        .getInputStream(zipFile.getEntry("java/" + name));
    int content;
    StringBuilder builder = new StringBuilder();
    ArrayList<String> list = new ArrayList<>();
    while ((content = fileInputStream.read()) != -1) {
      builder.append((char) content);
      if ((char) content == '\n') {
        list.add(builder.toString());
        builder.delete(0, builder.length());
      }
    }
    fileInputStream.close();
    list.stream()
        .forEach(s1 -> {
          if (s1.trim().startsWith("/*") || s1.trim().startsWith("*")) {
            System.out.println(s1);
          }
        });
  }

  private static int countOfTabs = 1;

  private static void testFileManager() throws IOException {
    Scanner scanner = new Scanner(System.in);
    parseFile(new File("C:\\"));
    String currentFile = "C:\\";
    while (true) {
      System.out.println("Enter name of nested directory or enter <- to go back(q to exit): ");
      String path = scanner.next();
      if (path.equals("q")) {
        System.exit(0);
      }
      if (path.equals("<-")) {
        if (!currentFile.equals("C:\\")) {
          countOfTabs--;
          currentFile = currentFile.substring(0, currentFile.lastIndexOf("\\"));
          parseFile(new File(currentFile));

        }
      } else {
        countOfTabs++;
        currentFile = currentFile + "\\" + path;
        File file = new File(currentFile);
        if (file.exists()) {
          parseFile(file);
        } else {
          System.out.println("There is no such file...");
          currentFile = currentFile.substring(0, currentFile.lastIndexOf("\\"));
        }
      }
    }
  }

  private static void parseFile(File file) throws IOException {
    System.out.println(file.getPath());
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        for (int i = 0; i < countOfTabs; i++) {
          System.out.print("\t");
        }
        BasicFileAttributes attributes = Files
            .readAttributes(Paths.get(child.getPath()), BasicFileAttributes.class);

        System.out.println(
            child.getName() + "\t\t\t" + attributes.lastModifiedTime() + "\t\t\t" + attributes
                .size());
      }
    }
  }

  private static void testBuffer() throws IOException {
    SomeBuffer someBuffer = new SomeBuffer();
    someBuffer.read(
        "C:\\projects\\epamHomework\\homeworkkoshelia\\ioNio\\src\\main\\resources\\stream.txt");
    ByteBuffer byteBuffer = ByteBuffer.allocate(10);
    byteBuffer.putChar('a');
    byteBuffer.flip();
    someBuffer.write(byteBuffer,
        "C:\\projects\\epamHomework\\homeworkkoshelia\\ioNio\\src\\main\\resources\\testWrite.txt");
  }

}
