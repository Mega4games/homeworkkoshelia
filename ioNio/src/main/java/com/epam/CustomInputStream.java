package com.epam;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

public class CustomInputStream extends PushbackInputStream {

  public CustomInputStream(InputStream in, int size) {
    super(in, size);
  }

  private void ensureOpen() throws IOException {
    if (in == null)
      throw new IOException("Stream closed");
  }


  public int readWithPushback(byte[] b, int start, int end) throws IOException {
    ensureOpen();
    if (b == null) {
      throw new NullPointerException();
    } else if (start < 0 || end < 0 || end > b.length - start) {
      throw new IndexOutOfBoundsException();
    } else if (end == 0) {
      return 0;
    }

    int pushedBytes = buf.length - pos;
    if (pushedBytes > 0) {
      if (end < pushedBytes) {
        pushedBytes = end;
      }
      System.arraycopy(buf, pos, b, start, pushedBytes);
      pos += pushedBytes;
      start += pushedBytes;
      end -= pushedBytes;
    }
    if (end > 0) {
      end = super.read(b, start, end);
      if (end == -1) {
        return pushedBytes == 0 ? -1 : pushedBytes;
      }
      return pushedBytes + end;
    }
    return pushedBytes;
  }


  public void pushBack(byte[] src, int start, int end) throws IOException {
    if (end > pos) {
      throw new IOException("Push back buffer is full");
    }
    pos -= end;
    System.arraycopy(src, start, buf, pos, end);
  }
}
