package com.epam;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.GatheringByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;

public class SomeBuffer {

  public void read(String path) throws IOException {
    Path filePath = Paths.get(path);
    SeekableByteChannel channel = Files.newByteChannel(filePath);
    ByteBuffer byteBuffer = ByteBuffer.allocate(10);

    Charset charset = Charset.forName("US-ASCII");
    while (channel.read(byteBuffer) > 0) {
      byteBuffer.rewind();
      System.out.print(charset.decode(byteBuffer));
      byteBuffer.flip();
    }
  }

  public void write(ByteBuffer buffer, String path) throws IOException {
    Set options = new HashSet();
    options.add(StandardOpenOption.CREATE);
    options.add(StandardOpenOption.APPEND);

    Path file = Paths.get(path);


    SeekableByteChannel byteChannel = Files.newByteChannel(file, options);
    byteChannel.write(buffer);
  }


}
