package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class Party {

  private static ArrayList<Participant> participants = new ArrayList<Participant>();

  static {
    participants.add(new Participant("Alice", 1));
    participants.add(new Participant("Bob", 2));
    participants.add(new Participant("Max", 3));
    participants.add(new Participant("John", 4));
    participants.add(new Participant("Tom", 5));
    participants.add(new Participant("Alina", 6));
    participants.add(new Participant("Hanna", 7));
    participants.add(new Participant("Mark", 8));
    participants.add(new Participant("Sophia", 9));
    participants.add(new Participant("Vova", 10));
  }

  private static final Random RANDOM = new Random();

  public static void main(String[] args) throws IOException {
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
      while (true) {
        System.out.println("Do you want to start the rumor? yes/no");
        if (reader.readLine().equals("yes")) {
          calculte();
        } else {
          System.exit(0);
        }
        for (int i = 0; i < participants.size(); i++) {
          participants.get(i).setHeardRumour(false);
        }
      }
    }
  }

  public static void calculte() {
    int peopleHeard = 1;
    participants.get(1).setHeardRumour(true); // Bob starts the rumour
    int choose = RANDOM.nextInt(8) + 2; // everyone except Bob and Alice
    int previousChoose = choose;
    Participant previous = participants.get(choose);
    peopleHeard++;
    participants.get(choose).setFromWho(participants.get(0));
    participants.get(choose).setHeardRumour(true);
    for (int i = 0; i < participants.size(); i++) {
      while (previousChoose == (choose = RANDOM.nextInt(participants.size()-1) + 1)) {
        participants.get(choose).setFromWho(previous);
      }
      previousChoose = choose;
      if (!participants.get(choose).isHeardRumour()) {
        participants.get(choose).setHeardRumour(true);
        peopleHeard++;
      } else {
        break;
      }
      previous = participants.get(choose);
    }
    System.out.println("Expected number of people who heard the rumour " + peopleHeard);
    System.out.println(
        "Expected probability " + ((double) (participants.size() - peopleHeard) / (participants.size() - 1)) * 100 + " %.");

  }

}
