package com.epam;

public class Participant {

  private boolean heardRumour = false;

  private String name;

  private int id;

  public Participant(String name, int id) {
    this.name = name;
    this.id = id;
  }

  public boolean isHeardRumour() {
    return heardRumour;
  }

  public void setHeardRumour(boolean heardRumour) {
    this.heardRumour = heardRumour;
  }

  public String getName() {
    return name;
  }

  private Participant fromWho;

  public Participant getFromWho() {
    return fromWho;
  }

  public void setFromWho(Participant fromWho) {
    this.fromWho = fromWho;
  }

  public int getId() {
    return id;
  }
}
