package com.epam;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainOperations {

  private StringBuilder allText = new StringBuilder();

  public void readFromFile() throws IOException {
    BufferedReader reader = new BufferedReader(
        new FileReader("C:\\projects\\epamHomework\\homeworkkoshelia\\Text.txt"));
    String s;
    while ((s = reader.readLine()) != null) {
      allText.append(s);
    }
    //task1();
    //task2();
    //task3();
    //task4();
    //task5();
    //task6();
    //task7();
    //task8();
    //task9();
    //task10();
    //task11();
    //task12();
    //task13();
    //task14();
    //task15();
    //task16();
  }

  //Знайти найбільшу кількість речень тексту, в яких є однакові слова.
  private void task1() {
    int count = 0;
    long numberWords = 0;
    String[] strs = allText.toString().split("\\.?!");
    for (int i = 0; i < strs.length; i++) {
      numberWords = Stream.of(strs[i].split(" "))
          .map(String::toLowerCase)
          .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
          .entrySet().stream()
          .filter(e -> e.getValue() > 1)
          .count();
      if (numberWords > 0) {
        count++;
      }
    }
    System.out.println(count);
  }

  //Вивести всі речення заданого тексту у порядку зростання кількості слів у
  //кожному з них.
  private void task2() {
    Stream.of(allText.toString().split("[\\.?!]"))
        .collect(Collectors.toMap(w -> (w), w -> w.split(" ").length))
        .entrySet().stream()
        .sorted(Comparator.comparing(Entry::getValue))
        .forEach(System.out::println);
  }

  //Знайти таке слово у першому реченні, якого немає ні в одному з інших
  //речень.
  private void task3() {
    String[] strs = allText.toString().split("[\\.?!]");
    ArrayList<String> otherSentences = new ArrayList<>();
    for (int i = 1; i < strs.length; i++) {
      otherSentences.add(strs[i]);
    }
    String[] words = strs[0].split(" ");
    String result = Stream.of(words)
        .map(String::toLowerCase)
        .filter(s -> (otherSentences.stream().filter(s1 -> s1.contains(s)).findAny().orElse(null)
            == null))
        .findAny().get();
    System.out.println(result);
  }

  //У всіх запитальних реченнях тексту знайти і надрукувати без повторів
  //слова заданої довжини.
  private void task4() {
    String[] strings = allText.toString().split("[\\.!]");
    List<String> questionList = Stream.of(strings)
        .filter(s -> s.contains("?"))
        .map(s -> s.substring(0, s.indexOf('?')))
        .collect(Collectors.toList());
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter the length of words: ");
    int length = scan.nextInt();

    for (String s : questionList) {
      Stream.of(s.split(" "))
          .filter(s1 -> s1.length() == length)
          .distinct()
          .forEach(System.out::println);
    }
  }


  //У кожному реченні тексту поміняти місцями перше слово, що починається
  //на голосну букву з найдовшим словом.
  private void task5() {
    String[] strings = allText.toString().split("[\\.?!]");
    List<String> longestWords;
    String firstVovel;
    Pattern vovel = Pattern.compile("^[АаЕеУуОоІі]");
    String[] words;
    for (int i = 0; i < strings.length; i++) {
      words = strings[i].split(" ");
      longestWords = Stream.of(words)
          .collect(Collectors.groupingBy(String::length))
          .entrySet()
          .stream()
          .max(Map.Entry.comparingByKey())
          .map(Map.Entry::getValue)
          .get();
      firstVovel = Stream.of(words)
          .filter(s -> vovel.matcher(s).find())
          .findFirst()
          .orElse(null);
      if (firstVovel != null) {
        System.out.println(firstVovel + " " + longestWords.get(0));
        for (int j = 0; j < words.length; j++) {
          if (words[j].equals(firstVovel)) {
            words[j] = longestWords.get(0);
            continue;
          }
          if (words[j].equals(longestWords.get(0))) {
            words[j] = firstVovel;
          }
        }
        Stream.of(words)
            .forEach(s -> System.out.print(s + " "));
        System.out.println();
      }
    }
  }

  //Надрукувати слова тексту в алфавітному порядку по першій букві. Слова,
  //що починаються з нової букви, друкувати з абзацного відступу.
  private void task6() {
    String[] words = allText.toString().split("[?!\\. ]");
    List<String> list = Stream.of(words)
        .map(String::toLowerCase)
        .sorted(Comparator.comparing(String::toString))
        .collect(Collectors.toList());
    System.out.println(list.get(0));
    for (int i = 1; i < list.size(); i++) {
      if (list.get(i).toCharArray()[0] != list.get(i - 1).toCharArray()[0]) {
        System.out.println("\t" + list.get(i));
      } else {
        System.out.println(list.get(i));
      }
    }
  }

  //Відсортувати слова тексту за зростанням відсотку голосних букв
  //(співвідношення кількості голосних до загальної кількості букв у слові).
  private void task7() {
    String[] words = allText.toString().replace(",", "").split("[?!\\. ]");
    Stream.of(words)
        .sorted((o1, o2) -> {
          Double percent1 =
              (double) o1.toLowerCase().replaceAll("[^АаЕеУуОоІіи]", "").length() / (o1.length());
          Double percent2 =
              (double) o2.toLowerCase().replaceAll("[^АаЕеУуОоІіи]", "").length() / (o2.length());
          return percent2.compareTo(percent1);
        })
        .collect(Collectors.toList())
        .forEach(System.out::println);
  }

  //8.Слова тексту, що починаються з голосних букв,
  // відсортувати в алфавітному порядку по першій приголосній букві слова.
  private void task8() {
    String[] words = allText.toString().replace(",", "").split("[?!\\. ]");
    Pattern vovel = Pattern.compile("^[АаЕеУуОоІі]");
    Stream.of(words)
        .filter(s -> (vovel.matcher(s)).find())
        .sorted(new Comparator<String>() {
          @Override
          public int compare(String o1, String o2) {
            return o1.replaceAll("[АаЕеУуОоІі]", "").compareTo(o2.replaceAll("[АаЕеУуОоІі]", ""));
          }
        })
        .forEach(System.out::println);
  }

  //9.	Всі слова тексту відсортувати за зростанням кількості заданої
  // букви у слові. Слова з однаковою кількістю розмістити у алфавітному порядку.
  private void task9() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Введіть задану букву");
    String ch = scanner.next();
    String[] words = allText.toString().replace(",", "").split("[?!\\. ]");
    Stream.of(words)
        .sorted()
        .sorted(Comparator.comparing(s -> (s.length() - s.replaceAll(ch, "").length())))
        .forEach(System.out::println);
  }

  //13.	Відсортувати слова у тексті за спаданням
  // кількості входжень заданого символу, а у випадку рівності – за алфавітом.
  private void task13() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Введіть задану букву");
    String ch = scanner.next();
    String[] words = allText.toString().replace(",", "").split("[?!\\. ]");
    Stream.of(words)
        .sorted()
        .sorted((o1, o2) -> {
          if (o1.replaceAll("[^" + ch + "]", "").length() > o2.replaceAll("[^" + ch + "]", "")
              .length()) {
            return -1;
          }
          if (o1.replaceAll("[^" + ch + "]", "").length() < o2.replaceAll("[^" + ch + "]", "")
              .length()) {
            return 1;
          }
          return 0;
        })
        .forEach(System.out::println);
  }

  //10.	Є текст і список слів. Для кожного слова з заданого списку знайти,
  // скільки разів воно зустрічається у кожному реченні, і відсортувати слова за спаданням загальної кількості входжень.
  private void task10() {
    String[] words = allText.toString().replace(",", "").split("[?!\\. ]");
    ArrayList<String> listWords = new ArrayList<>();
    listWords.add("а");
    listWords.add("б");
    listWords.add("що");
    listWords.add("ви");
    listWords.stream()
        .sorted(Comparator.comparing(s -> Stream.of(words)
            .filter(s1 -> !s1.equals(s))
            .count()))
        .forEach(System.out::println);
  }

  //11.	У кожному реченні тексту видалити підрядок максимальної довжини,
  // що починається і закінчується заданими символами.
  private void task11() {
    String[] strings = allText.toString().split("[\\.?!]");
    System.out.println("Case sensitive.");
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter first word:");
    String first = scanner.next();
    System.out.println("Enter last word:");
    String second = scanner.next();
    StringBuilder builder = new StringBuilder();
    Stream.of(strings)
        .map(s -> {
          builder.delete(0, builder.length());
          builder.append(s);
          System.out.println(builder.toString());
          if (s.contains(first) && s.contains(second) && s.indexOf(first) < s.lastIndexOf(second)) {
            builder.replace(s.indexOf(first), s.lastIndexOf(second) + second.length(), "");
          }
          return builder.toString();
        }).forEach(System.out::println);

  }

  //12.	З тексту видалити всі слова заданої довжини, що починаються на приголосну букву.
  private void task12() {
    Pattern notVovel = Pattern.compile("^[^АаЕеУуОоІі]");
    String[] words = allText.toString().replace(",", "").split("[?!\\. ]");
    Scanner scanner = new Scanner(System.in);
    int count = scanner.nextInt();

    List<String> deletedWords = Stream.of(words)
        .filter(s -> s.length() == count)
        .filter(s -> notVovel.matcher(s).find())
        .distinct()
        .collect(Collectors.toList());
    deletedWords.stream()
        .forEach(System.out::println);
    String text = allText.toString();
    deletedWords.stream()
        .forEach(s -> System.out.println(text.replaceAll(s, "")));
  }

  //14.	У заданому тексті знайти підрядок максимальної довжини, який є
  // паліндромом, тобто, читається зліва на право і справа на ліво однаково.
  private void task14() {
    String[] words = allText.toString().replace(",", "").split("[?!\\. ]");

    String palindrome = Stream.of(words)
        .map(String::toLowerCase)
        .filter(s -> new StringBuilder(s).reverse().toString().equals(s) && s.length() > 1)
        .sorted((o1, o2) -> {
          if (o1.length() > o2.length()) {
            return -1;
          }
          if (o1.length() < o2.length()) {
            return 1;
          }
          return 0;
        }).findFirst().orElse(null);
    if (palindrome != null) {
      System.out.println(palindrome);
    } else {
      System.out.println("There is no palindrome.");
    }

  }

  //15.Перетворити кожне слово у тексті, видаливши з нього всі наступні
  //(попередні) входження першої (останньої) букви цього слова.
  private void task15() {
    String[] words = allText.toString().replace(",", "").split("[?!\\. ]");
    System.out.println();
    Stream.of(words)
        .map(s -> {
          StringBuilder res = new StringBuilder();
          res.append(s.toCharArray()[0]);
          for (int i = 1; i < s.toCharArray().length; i++) {
            if (s.toCharArray()[i] != s.toCharArray()[0]) {
              res.append(s.toCharArray()[i]);
            }
          }
          return res.toString();
        })
        .forEach(System.out::println);
  }

  //16.У певному реченні тексту слова заданої довжини замінити вказаним
  //підрядком, довжина якого може не співпадати з довжиною слова.
  private void task16() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter size: ");
    int size = scanner.nextInt();
    System.out.println("Enter sentence: ");
    String sent = scanner.next();
    String[] strings = allText.toString().split("[\\.?!]");
    Stream.of(strings)
        .map(s -> {
          StringBuilder stringBuilder = new StringBuilder(s);
          Stream.of(s.split(" "))
              .forEach(s1 -> {
                if (s1.contains(",")) {
                  if (s1.length() - 1 == size) {
                    stringBuilder.replace(stringBuilder.indexOf(s1.substring(0, s1.length())),
                        stringBuilder.indexOf(s1.substring(0, s1.length())) + s1.length(), sent);
                  }
                } else {
                  if (s1.length() == size) {
                    stringBuilder
                        .replace(stringBuilder.indexOf(s1), stringBuilder.indexOf(s1) + s1.length(),
                            sent);
                  }
                }

              });
          return stringBuilder.toString();
        }).forEach(System.out::println);

  }

}
