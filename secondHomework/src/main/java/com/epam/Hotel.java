package com.epam;

import java.util.ArrayList;

public class Hotel extends DetachedHouse {

  private boolean restaurant;
  private ArrayList<String> additionalServices;

  public Hotel(int price, int rentalPrice, int roomCount, double area, int destToInfrastruct,
      String address, int floorCount, double gardenArea, int garageCount, boolean swimPool,
      boolean restaurant, ArrayList<String> additionalServices) {
    super(price, rentalPrice, roomCount, area, destToInfrastruct, address, floorCount, gardenArea,
        garageCount, swimPool);
    this.restaurant = restaurant;
    this.additionalServices = additionalServices;
  }

  public boolean hasRestaurant() {
    return restaurant;
  }

  public void setRestaurant(boolean restaurant) {
    this.restaurant = restaurant;
  }

  public ArrayList<String> getAdditionalServices() {
    return additionalServices;
  }

  public void setAdditionalServices(ArrayList<String> additionalServices) {
    this.additionalServices = additionalServices;
  }

  @Override
  public String toString() {
    return "Hotel{" +
        "restaurant=" + restaurant +
        ", additionalServices=" + additionalServices +
        '}';
  }
}
