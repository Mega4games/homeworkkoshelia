package com.epam;

public class Dwelling {

  private int price;
  private int rentalPrice;
  private int roomCount;
  private double area;
  private int destToInfrastruct;
  private String address;

  public Dwelling(int price, int rentalPrice, int roomCount, double area, int destToInfrastruct,
      String address) {
    this.price = price;
    this.rentalPrice = rentalPrice;
    this.roomCount = roomCount;
    this.area = area;
    this.destToInfrastruct = destToInfrastruct;
    this.address = address;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getRentalPrice() {
    return rentalPrice;
  }

  public void setRentalPrice(int rentalPrice) {
    this.rentalPrice = rentalPrice;
  }

  public int getRoomCount() {
    return roomCount;
  }

  public void setRoomCount(int roomCount) {
    this.roomCount = roomCount;
  }

  public double getArea() {
    return area;
  }

  public void setArea(double area) {
    this.area = area;
  }

  public int getDestToInfrastruct() {
    return destToInfrastruct;
  }

  public void setDestToInfrastruct(int destToInfrastruct) {
    this.destToInfrastruct = destToInfrastruct;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return "Dwelling{" +
        "price=" + price +
        ", rentalPrice=" + rentalPrice +
        ", roomCount=" + roomCount +
        ", area=" + area +
        ", destToInfrastruct=" + destToInfrastruct +
        ", address='" + address + '\'' +
        '}';
  }
}
