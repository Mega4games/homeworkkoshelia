package com.epam;

public class Flat extends Dwelling {

  private int floor;
  private String houseType;
  private boolean elevator;
  private String heatingType;

  public Flat(int price, int rentalPrice, int roomCount, double area, int destToInfrastruct,
      String address, int floor, String houseType, boolean elevator, String heatingType) {
    super(price, rentalPrice, roomCount, area, destToInfrastruct, address);
    this.floor = floor;
    this.houseType = houseType;
    this.elevator = elevator;
    this.heatingType = heatingType;
  }


  public int getFloor() {
    return floor;
  }

  public void setFloor(int floor) {
    this.floor = floor;
  }

  public String getHouseType() {
    return houseType;
  }

  public void setHouseType(String houseType) {
    this.houseType = houseType;
  }

  public boolean hasElevator() {
    return elevator;
  }

  public void setElevator(boolean elevator) {
    this.elevator = elevator;
  }

  public String getHeatingType() {
    return heatingType;
  }

  public void setHeatingType(String heatingType) {
    this.heatingType = heatingType;
  }

  @Override
  public String toString() {
    return "Flat{" +
        "floor=" + floor +
        ", houseType='" + houseType + '\'' +
        ", elevator=" + elevator +
        ", heatingType='" + heatingType + '\'' +
        '}';
  }
}
