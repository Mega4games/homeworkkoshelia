package com.epam;

public class PenthHouse extends Flat {

  private int floorCount;
  private boolean terracce;

  public PenthHouse(int price, int rentalPrice, int roomCount, double area, int destToInfrastruct,
      String address, int floor, String houseType, boolean elevator, String heatingType,
      int floorCount, boolean terracce) {
    super(price, rentalPrice, roomCount, area, destToInfrastruct, address, floor, houseType,
        elevator,
        heatingType);
    this.floorCount = floorCount;
    this.terracce = terracce;
  }


  public int getFloorCount() {
    return floorCount;
  }

  public void setFloorCount(int floorCount) {
    this.floorCount = floorCount;
  }

  public boolean hasTerracce() {
    return terracce;
  }

  public void setTerracce(boolean terracce) {
    this.terracce = terracce;
  }

  @Override
  public String toString() {
    return "PenthHouse{" +
        "floorCount=" + floorCount +
        ", terracce=" + terracce +
        '}';
  }
}
