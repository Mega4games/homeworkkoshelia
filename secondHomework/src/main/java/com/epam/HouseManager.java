package com.epam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HouseManager {

  public static ArrayList<Dwelling> houseList = new ArrayList<>();
  public static ArrayList<String> additionalServices = new ArrayList<>();

  static {
    additionalServices.add("WI-FI");
    additionalServices.add("SPA");
    additionalServices.add("Tennis court");
    houseList.add(new DetachedHouse(200000, 10000, 10, 400, 2, "Kopernyka",
        3, 130, 1, false));
    houseList.add(new DetachedHouse(300000, 15000, 15, 650, 3, "Bandera st.",
        4, 200, 2, true));
    houseList.add(new DetachedHouse(150000, 8000, 19, 400, 1, "Konovaltsia",
        3, 100, 1, false));
    houseList.add(new Flat(80000, 5000, 3, 80, 1, "Gorodotska",
        2, "Polski", true, "Central"));
    houseList.add(new Flat(120000, 9000, 3, 120, 1, "Yaponska",
        4, "Polski", true, "Central"));
    houseList.add(new Flat(70000, 5000, 2, 60, 2, "Naukova",
        1, "Soviet", true, "Individual"));
    houseList.add(new Hotel(10000000, 1000, 20, 1300, 5, "Luchakivska",
        7, 100, 3, true, true, additionalServices));
    houseList.add(new PenthHouse(150000, 15000, 5, 150, 2, "Zelena", 6,
        "Modern", true, "Individual", 2, true));
    houseList.add(new PenthHouse(190000, 16000, 6, 190, 4, "Zelena", 8,
        "Modern", true, "Individual", 1, true));
    houseList.add(new PenthHouse(115000, 12000, 5, 140, 2, "Pekarska", 7,
        "Modern", true, "Individual", 2, false));
  }

  public static void main(String[] args) {
    System.out.println(searchDwellingByDest(1));
    System.out.println(searchDwellingByRentalPrice(12000));
  }

  public static Dwelling searchDwellingByDest(final int dest) {
    return houseList.stream()
        .filter((Dwelling dwelling) -> (dwelling.getDestToInfrastruct() == dest)).findAny()
        .orElse(null);
  }

  public static Dwelling searchDwellingByRentalPrice(int rentalPrice) {
    return houseList.stream()
        .filter((Dwelling dwelling) -> (dwelling.getRentalPrice() == rentalPrice)).findAny()
        .orElse(null);
  }
}
