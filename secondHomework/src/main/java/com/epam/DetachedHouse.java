package com.epam;

public class DetachedHouse extends Dwelling {

  private int floorCount;
  private double gardenArea;
  private int garageCount;
  private boolean swimPool;

  public DetachedHouse(int price, int rentalPrice, int roomCount, double area,
      int destToInfrastruct, String address, int floorCount, double gardenArea,
      int garageCount, boolean swimPool) {
    super(price, rentalPrice, roomCount, area, destToInfrastruct, address);
    this.floorCount = floorCount;
    this.gardenArea = gardenArea;
    this.garageCount = garageCount;
    this.swimPool = swimPool;
  }


  public int getFloorCount() {
    return floorCount;
  }

  public void setFloorCount(int floorCount) {
    this.floorCount = floorCount;
  }

  public double getGardenArea() {
    return gardenArea;
  }

  public void setGardenArea(double gardenArea) {
    this.gardenArea = gardenArea;
  }

  public int getGarageCount() {
    return garageCount;
  }

  public void setGarageCount(int garageCount) {
    this.garageCount = garageCount;
  }

  public boolean hasSwimPool() {
    return swimPool;
  }

  public void setSwimPool(boolean swimPool) {
    this.swimPool = swimPool;
  }

  @Override
  public String toString() {
    return "DetachedHouse{" +
        "floorCount=" + floorCount +
        ", gardenArea=" + gardenArea +
        ", garageCount=" + garageCount +
        ", swimPool=" + swimPool +
        '}';
  }
}
