package com.epam;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class BinaryTreeMap<K, V> extends AbstractMap<K, V> {

  private int size = 0;

  private transient BinaryTreeMap.Pair<K,V> root;

  private static final boolean RED   = false;
  private static final boolean BLACK = true;

  public int size() {
    return size;
  }

  static final boolean valEquals(Object o1, Object o2) {
    return (o1==null ? o2==null : o1.equals(o2));
  }

  final BinaryTreeMap.Pair<K,V> getPair(Object key) {

    if (key == null)
      throw new NullPointerException();
    @SuppressWarnings("unchecked")
    Comparable<? super K> k = (Comparable<? super K>) key;
    BinaryTreeMap.Pair<K,V> p = root;
    while (p != null) {
      int cmp = k.compareTo(p.key);
      if (cmp < 0)
        p = p.left;
      else if (cmp > 0)
        p = p.right;
      else
        return p;
    }
    return null;
  }

  public V get(Object key) {
    BinaryTreeMap.Pair<K,V> p = getPair(key);
    return (p==null ? null : p.value);
  }



  public V put(K key, V value) {
    Pair<K,V> t = root;
    if (t == null) {
      root = new Pair<>(key, value, null);
      size = 1;
      return null;
    }
    int cmp;
    Pair<K,V> parent;

      if (key == null)
        throw new NullPointerException();
      @SuppressWarnings("unchecked")
      Comparable<? super K> k = (Comparable<? super K>) key;
      do {
        parent = t;
        cmp = k.compareTo(t.key);
        if (cmp < 0)
          t = t.left;
        else if (cmp > 0)
          t = t.right;
        else
          return t.setValue(value);
      } while (t != null);

    Pair<K,V> e = new Pair<>(key, value, parent);
    if (cmp < 0)
      parent.left = e;
    else
      parent.right = e;
    fixAfterInsertion(e);
    size++;
    return null;
  }

  private static <K,V> Pair<K,V> parentOf(Pair<K,V> p) {
    return (p == null ? null: p.parent);
  }

  private static <K,V> Pair<K,V> leftOf(Pair<K,V> p) {
    return (p == null) ? null: p.left;
  }

  private static <K,V> Pair<K,V> rightOf(Pair<K,V> p) {
    return (p == null) ? null: p.right;
  }

  private static <K,V> boolean colorOf(Pair<K,V> p) {
    return (p == null ? BLACK : p.color);
  }

  private static <K,V> void setColor(Pair<K,V> p, boolean c) {
    if (p != null)
      p.color = c;
  }

  private void rotateLeft(Pair<K,V> p) {
    if (p != null) {
      Pair<K,V> r = p.right;
      p.right = r.left;
      if (r.left != null)
        r.left.parent = p;
      r.parent = p.parent;
      if (p.parent == null)
        root = r;
      else if (p.parent.left == p)
        p.parent.left = r;
      else
        p.parent.right = r;
      r.left = p;
      p.parent = r;
    }
  }


  private void rotateRight(Pair<K,V> p) {
    if (p != null) {
      Pair<K,V> l = p.left;
      p.left = l.right;
      if (l.right != null) l.right.parent = p;
      l.parent = p.parent;
      if (p.parent == null)
        root = l;
      else if (p.parent.right == p)
        p.parent.right = l;
      else p.parent.left = l;
      l.right = p;
      p.parent = l;
    }
  }

  private void fixAfterInsertion(Pair<K,V> x) {
    x.color = RED;

    while (x != null && x != root && x.parent.color == RED) {
      if (parentOf(x) == leftOf(parentOf(parentOf(x)))) { // insert into left side
        Pair<K,V> y = rightOf(parentOf(parentOf(x)));
        if (colorOf(y) == RED) {
          setColor(parentOf(x), BLACK);
          setColor(y, BLACK);
          setColor(parentOf(parentOf(x)), RED);
          x = parentOf(parentOf(x));
        } else {
          if (x == rightOf(parentOf(x))) {
            x = parentOf(x);
            rotateLeft(x);
          }
          setColor(parentOf(x), BLACK);
          setColor(parentOf(parentOf(x)), RED);
          rotateRight(parentOf(parentOf(x)));
        }
      } else {
        Pair<K,V> y = leftOf(parentOf(parentOf(x)));
        if (colorOf(y) == RED) {
          setColor(parentOf(x), BLACK);
          setColor(y, BLACK);
          setColor(parentOf(parentOf(x)), RED);
          x = parentOf(parentOf(x));
        } else {
          if (x == leftOf(parentOf(x))) {
            x = parentOf(x);
            rotateRight(x);
          }
          setColor(parentOf(x), BLACK);
          setColor(parentOf(parentOf(x)), RED);
          rotateLeft(parentOf(parentOf(x)));
        }
      }
    }
    root.color = BLACK;
  }

  public V remove(Object key) {
    Pair<K,V> p = getPair(key);
    if (p == null)
      return null;

    V oldValue = p.value;
    deleteEntry(p);
    return oldValue;
  }

  static <K,V> BinaryTreeMap.Pair<K,V> successor(Pair<K,V> t) {
    if (t == null)
      return null;
    else if (t.right != null) {
      Pair<K,V> p = t.right;
      while (p.left != null)
        p = p.left;
      return p;
    } else {
      Pair<K,V> p = t.parent;
      Pair<K,V> ch = t;
      while (p != null && ch == p.right) {
        ch = p;
        p = p.parent;
      }
      return p;
    }
  }

  private void deleteEntry(Pair<K,V> p) {

    size--;

    // If strictly internal, copy successor's element to p and then make p
    // point to successor.
    if (p.left != null && p.right != null) {
      Pair<K,V> s = successor(p);
      p.key = s.key;
      p.value = s.value;
      p = s;
    } // p has 2 children

    // Start fixup at replacement node, if it exists.
    Pair<K,V> replacement = (p.left != null ? p.left : p.right);

    if (replacement != null) {
      // Link replacement to parent
      replacement.parent = p.parent;
      if (p.parent == null)
        root = replacement;
      else if (p == p.parent.left)
        p.parent.left  = replacement;
      else
        p.parent.right = replacement;

      // Null out links so they are OK to use by fixAfterDeletion.
      p.left = p.right = p.parent = null;

      // Fix replacement
      if (p.color == BLACK)
        fixAfterDeletion(replacement);
    } else if (p.parent == null) { // return if we are the only node.
      root = null;
    } else { //  No children. Use self as phantom replacement and unlink.
      if (p.color == BLACK)
        fixAfterDeletion(p);

      if (p.parent != null) {
        if (p == p.parent.left)
          p.parent.left = null;
        else if (p == p.parent.right)
          p.parent.right = null;
        p.parent = null;
      }
    }
  }

  private void fixAfterDeletion(Pair<K,V> x) {
    while (x != root && colorOf(x) == BLACK) {
      if (x == leftOf(parentOf(x))) {
        Pair<K,V> sib = rightOf(parentOf(x));

        if (colorOf(sib) == RED) {
          setColor(sib, BLACK);
          setColor(parentOf(x), RED);
          rotateLeft(parentOf(x));
          sib = rightOf(parentOf(x));
        }

        if (colorOf(leftOf(sib))  == BLACK &&
            colorOf(rightOf(sib)) == BLACK) {
          setColor(sib, RED);
          x = parentOf(x);
        } else {
          if (colorOf(rightOf(sib)) == BLACK) {
            setColor(leftOf(sib), BLACK);
            setColor(sib, RED);
            rotateRight(sib);
            sib = rightOf(parentOf(x));
          }
          setColor(sib, colorOf(parentOf(x)));
          setColor(parentOf(x), BLACK);
          setColor(rightOf(sib), BLACK);
          rotateLeft(parentOf(x));
          x = root;
        }
      } else { // symmetric
        Pair<K,V> sib = leftOf(parentOf(x));

        if (colorOf(sib) == RED) {
          setColor(sib, BLACK);
          setColor(parentOf(x), RED);
          rotateRight(parentOf(x));
          sib = leftOf(parentOf(x));
        }

        if (colorOf(rightOf(sib)) == BLACK &&
            colorOf(leftOf(sib)) == BLACK) {
          setColor(sib, RED);
          x = parentOf(x);
        } else {
          if (colorOf(leftOf(sib)) == BLACK) {
            setColor(rightOf(sib), BLACK);
            setColor(sib, RED);
            rotateLeft(sib);
            sib = leftOf(parentOf(x));
          }
          setColor(sib, colorOf(parentOf(x)));
          setColor(parentOf(x), BLACK);
          setColor(leftOf(sib), BLACK);
          rotateRight(parentOf(x));
          x = root;
        }
      }
    }

    setColor(x, BLACK);
  }


  static final class Pair<K,V> implements Map.Entry<K,V> {
    K key;
    V value;
    BinaryTreeMap.Pair<K,V> left;
    BinaryTreeMap.Pair<K,V> right;
    BinaryTreeMap.Pair<K,V> parent;
    boolean color = BLACK;

    /**
     * Make a new cell with given key, value, and parent, and with
     * {@code null} child links, and BLACK color.
     */
    Pair(K key, V value, BinaryTreeMap.Pair<K,V> parent) {
      this.key = key;
      this.value = value;
      this.parent = parent;
    }

    /**
     * Returns the key.
     *
     * @return the key
     */
    public K getKey() {
      return key;
    }

    /**
     * Returns the value associated with the key.
     *
     * @return the value associated with the key
     */
    public V getValue() {
      return value;
    }

    /**
     * Replaces the value currently associated with the key with the given
     * value.
     *
     * @return the value associated with the key before this method was
     *         called
     */
    public V setValue(V value) {
      V oldValue = this.value;
      this.value = value;
      return oldValue;
    }

    public boolean equals(Object o) {
      if (!(o instanceof Map.Entry))
        return false;
      Map.Entry<?,?> e = (Map.Entry<?,?>)o;

      return valEquals(key,e.getKey()) && valEquals(value,e.getValue());
    }

    public int hashCode() {
      int keyHash = (key==null ? 0 : key.hashCode());
      int valueHash = (value==null ? 0 : value.hashCode());
      return keyHash ^ valueHash;
    }

    public String toString() {
      return key + "=" + value;
    }
  }

  public Set<Entry<K, V>> entrySet() {
    return null;
  }

  public V getOrDefault(Object key, V defaultValue) {
    return null;
  }

  public void forEach(BiConsumer<? super K, ? super V> action) {

  }

  public void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {

  }

  public V putIfAbsent(K key, V value) {
    return null;
  }

  public boolean remove(Object key, Object value) {
    return false;
  }

  public boolean replace(K key, V oldValue, V newValue) {
    return false;
  }

  public V replace(K key, V value) {
    return null;
  }

  public V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
    return null;
  }

  public V computeIfPresent(K key,
      BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
    return null;
  }

  public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
    return null;
  }

  public V merge(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
    return null;
  }
}
