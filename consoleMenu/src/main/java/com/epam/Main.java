package com.epam;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

  private static BinaryTreeMap<Integer, String> menu = new BinaryTreeMap<>();

  public static void main(String[] args) {

    menu.put(0, Menu.Add.getDescription());
    menu.put(1, Menu.Delete.getDescription());
    menu.put(2, Menu.Get.getDescription());
    menu.put(3, Menu.Size.getDescription());
    menu.put(4, "5 - Other Function");

    System.out.println("------Menu with help of TreeMap------");

    for (int i = 0; i < menu.size(); i++) {
      System.out.println(menu.get(i));
    }

    System.out.println("------After Deletion------");

    menu.remove(4);

    for (int i = 0; i < menu.size(); i++) {
      System.out.println(menu.get(i));
    }
  }

}
