package com.epam;

import java.util.TreeMap;

public enum Menu {
  Add("1 - Додати елемент"),
  Delete("2 - Видалити елемент"),
  Get("3 - Отримати елемент"),
  Size("4 - Розмір"),
  Quit("5 - Вийти");

  private String description;

  Menu(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}

