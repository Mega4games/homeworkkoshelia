package com.epam;

import com.epam.dom.DOMParserUser;
import com.epam.model.Medicine;
import com.epam.sax.SAXParserUser;
import com.epam.stax.StAXReader;
import java.io.File;
import java.util.Collections;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    File xml = new File("src\\main\\resources\\xml\\medicineXML.xml");
    File xsd = new File("src\\main\\resources\\xml\\medicineXSD.xsd");

    if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            SAX
      printList(SAXParserUser.parseBeers(xml, xsd), "SAX");

//            StAX
      printList(StAXReader.parseMedicines(xml, xsd), "StAX");

//            DOM
      printList(DOMParserUser.getMedicines(xml, xsd), "DOM");
    }
  }

  private static boolean checkIfXSD(File xsd) {
    return ExtensionChecker.isXSD(xsd);
  }

  private static boolean checkIfXML(File xml) {
    return ExtensionChecker.isXML(xml);
  }

  private static void printList(List<Medicine> medicines, String parserName) {
    Collections.sort(medicines, new MedicineComparator());
    System.out.println(parserName);
    for (Medicine medicine : medicines) {
      System.out.println(medicine);
    }
  }
}
