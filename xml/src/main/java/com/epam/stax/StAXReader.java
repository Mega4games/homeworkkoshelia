package com.epam.stax;

import com.epam.model.Certificate;
import com.epam.model.Medicine;
import com.epam.model.Package;
import com.epam.model.Versions;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAXReader {

  private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

  public static List<Medicine> parseMedicines(File xml, File xsd) {
    List<Medicine> beerList = new ArrayList<>();
    Medicine medicine = null;
    Versions versions = null;
    Certificate certificate = null;
    Package aPackage = null;
    List<String> analogs = null;
    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "medicine":
              medicine = new Medicine();

              Attribute idAttr = startElement.getAttributeByName(new QName("medicineId"));
              if (idAttr != null) {
                medicine.setMedicineId(Integer.parseInt(idAttr.getValue()));
              }
              break;
            case "name":
              xmlEvent = xmlEventReader.nextEvent();
              assert medicine != null;
              medicine.setName(xmlEvent.asCharacters().getData());
              break;
            case "pharm":
              xmlEvent = xmlEventReader.nextEvent();
              assert medicine != null;
              medicine.setPharm(xmlEvent.asCharacters().getData());
              break;
            case "group":
              xmlEvent = xmlEventReader.nextEvent();
              assert medicine != null;
              medicine.setGroup(xmlEvent.asCharacters().getData());
              break;
            case "analogs":
              xmlEvent = xmlEventReader.nextEvent();
              analogs = new ArrayList<>();
              break;
            case "analog":
              xmlEvent = xmlEventReader.nextEvent();
              assert analogs != null;
              analogs.add(xmlEvent.asCharacters().getData());
              break;
            case "versions":
              xmlEvent = xmlEventReader.nextEvent();
              versions = new Versions();
              break;
            case "consistency":
              xmlEvent = xmlEventReader.nextEvent();
              assert versions != null;
              versions.setConsistency(xmlEvent.asCharacters().getData());
              break;
            case "certificate":
              xmlEvent = xmlEventReader.nextEvent();
              assert versions != null;
              certificate = new Certificate();
              break;
            case "number":
              xmlEvent = xmlEventReader.nextEvent();
              assert certificate != null;
              certificate.setNumber(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "issueDate":
              xmlEvent = xmlEventReader.nextEvent();
              assert certificate != null;
              try {
                certificate.setIssueDate(formatter.parse(xmlEvent.asCharacters().getData()));
              } catch (ParseException e) {
                System.out.println("Wrong parsing");
              }
              break;
            case "finishDate":
              xmlEvent = xmlEventReader.nextEvent();
              assert certificate != null;
              try {
                certificate.setFinishDate(formatter.parse(xmlEvent.asCharacters().getData()));
              } catch (ParseException e) {
                System.out.println("Wrong parsing");
              }
              break;
            case "organization":
              xmlEvent = xmlEventReader.nextEvent();
              assert certificate != null;
              certificate.setOrganization(xmlEvent.asCharacters().getData());
              break;
            case "package":
              xmlEvent = xmlEventReader.nextEvent();
              aPackage = new Package();
              break;
            case "type":
              xmlEvent = xmlEventReader.nextEvent();
              assert aPackage != null;
              aPackage.setType(xmlEvent.asCharacters().getData());
              break;
            case "count":
              xmlEvent = xmlEventReader.nextEvent();
              assert aPackage != null;
              aPackage.setCount(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "price":
              xmlEvent = xmlEventReader.nextEvent();
              assert aPackage != null;
              aPackage.setPrice(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "dossage":
              xmlEvent = xmlEventReader.nextEvent();
              assert versions != null;
              versions.setDossage(xmlEvent.asCharacters().getData());
              assert certificate != null;
              versions.setCertificate(certificate);
              assert aPackage != null;
              versions.setaPackage(aPackage);
              medicine.setVersions(versions);
              medicine.setAnalogs(analogs);
              break;
          }
        }

        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();

          if (endElement.getName().getLocalPart().equals("medicine")) {
            beerList.add(medicine);
          }
        }
      }

    } catch (FileNotFoundException | XMLStreamException e) {
      e.printStackTrace();
    }
    return beerList;
  }
}
