package com.epam.model;

public class Versions {
  private String consistency;
  private Certificate certificate;
  private Package aPackage;
  private String dossage;


  public Versions() {

  }

  public Versions(String consistency, Package aPackage, String dossage) {
    this.consistency = consistency;
    this.aPackage = aPackage;
    this.dossage = dossage;
  }

  public String getConsistency() {
    return consistency;
  }

  public void setConsistency(String consistency) {
    this.consistency = consistency;
  }

  public Package getaPackage() {
    return aPackage;
  }

  public void setaPackage(Package aPackage) {
    this.aPackage = aPackage;
  }

  public String getDossage() {
    return dossage;
  }

  public void setDossage(String dossage) {
    this.dossage = dossage;
  }

  public Certificate getCertificate() {
    return certificate;
  }

  public void setCertificate(Certificate certificate) {
    this.certificate = certificate;
  }
}
