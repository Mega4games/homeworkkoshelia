package com.epam.model;

import java.util.List;

public class Medicine {

  private int medicineId;
  private String name;
  private String Pharm;
  private String Group;
  private List<String> analogs;
  private Versions versions;

  public Medicine() {

  }


  public int getMedicineId() {
    return medicineId;
  }

  public void setMedicineId(int medicineId) {
    this.medicineId = medicineId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPharm() {
    return Pharm;
  }

  public void setPharm(String pharm) {
    Pharm = pharm;
  }

  public String getGroup() {
    return Group;
  }

  public void setGroup(String group) {
    Group = group;
  }

  public List<String> getAnalogs() {
    return analogs;
  }

  public void setAnalogs(List<String> analogs) {
    this.analogs = analogs;
  }

  public Versions getVersions() {
    return versions;
  }

  public void setVersions(Versions versions) {
    this.versions = versions;
  }

  @Override
  public String toString() {
    return "Medicine{" +
        "medicineId=" + medicineId +
        ", name='" + name + '\'' +
        ", Pharm='" + Pharm + '\'' +
        ", Group='" + Group + '\'' +
        ", analogs=" + analogs +
        ", consistency=" + versions.getConsistency() +
        ", certificate: number=" + versions.getCertificate().getNumber() +
        ", issueDate=" + versions.getCertificate().getIssueDate() +
        ", finishDate=" + versions.getCertificate().getFinishDate() +
        ", organization=" + versions.getCertificate().getOrganization() +
        ", package: type=" + versions.getaPackage().getType() +
        ", count=" + versions.getaPackage().getCount() +
        ", price=" + versions.getaPackage().getPrice() +
        ", dossage=" + versions.getDossage() +
        '}';
  }
}
