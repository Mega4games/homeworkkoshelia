package com.epam.dom;

import com.epam.model.Certificate;
import com.epam.model.Medicine;
import com.epam.model.Package;
import com.epam.model.Versions;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMDocReader {

  public List<Medicine> readDoc(Document doc) {
    doc.getDocumentElement().normalize();
    List<Medicine> medicines = new ArrayList<>();
    NodeList nodeList = doc.getElementsByTagName("medicine");

    for (int i = 0; i < nodeList.getLength(); i++) {
      Medicine medicine = new Medicine();
      Versions versions;
      List<String> analogs;

      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;

        medicine.setMedicineId(Integer.parseInt(element.getAttribute("medicineId")));
        medicine.setName(element.getElementsByTagName("name").item(0).getTextContent());
        medicine.setPharm(element.getElementsByTagName("pharm").item(0).getTextContent());
        medicine.setGroup(element.getElementsByTagName("group").item(0).getTextContent());

        analogs = getAnalogs(element.getElementsByTagName("analogs"));
        medicine.setAnalogs(analogs);
        versions = getVersions(element.getElementsByTagName("versions"));
        medicine.setVersions(versions);

        medicines.add(medicine);
      }
    }
    return medicines;
  }

  private List<String> getAnalogs(NodeList nodes) {
    List<String> analogs = new ArrayList<>();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      NodeList nodeList = element.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          Element el = (Element) node;
          analogs.add(el.getTextContent());
        }
      }
    }

    return analogs;
  }

  private Versions getVersions(NodeList nodes) {
    Versions versions = new Versions();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      versions.setConsistency(element.getElementsByTagName("consistency").item(0).getTextContent());
      Certificate certificate = getCertificate(element.getElementsByTagName("certificate"));
      versions.setCertificate(certificate);
      Package aPackage = getPack(element.getElementsByTagName("package"));
      versions.setaPackage(aPackage);
      versions.setDossage(element.getElementsByTagName("dossage").item(0).getTextContent());
    }

    return versions;
  }

  private Package getPack(NodeList nodeList) {
    Package aPackage = new Package();
    if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodeList.item(0);
      aPackage.setType(element.getElementsByTagName("type").item(0).getTextContent());
      aPackage.setCount(
          Integer.parseInt(element.getElementsByTagName("count").item(0).getTextContent()));
      aPackage.setPrice(
          Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
    }
    return aPackage;
  }

  private Certificate getCertificate(NodeList nodeList) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    Certificate certificate = new Certificate();
    if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodeList.item(0);
      certificate.setNumber(
          Integer.parseInt(element.getElementsByTagName("number").item(0).getTextContent()));
      try {
        certificate.setIssueDate(
            formatter.parse(element.getElementsByTagName("issueDate").item(0).getTextContent()));
        certificate.setFinishDate(
            formatter.parse(element.getElementsByTagName("finishDate").item(0).getTextContent()));
      } catch (ParseException e) {
        System.out.println("Wrong parsing");
      }

      certificate
          .setOrganization(element.getElementsByTagName("organization").item(0).getTextContent());
    }
    return certificate;
  }


}