package com.epam;

import com.epam.model.Medicine;
import java.util.Comparator;

public class MedicineComparator implements Comparator<Medicine> {
  @Override
  public int compare(Medicine o1, Medicine o2) {
    return Double.compare(o1.getVersions().getaPackage().getPrice(), o2.getVersions().getaPackage().getPrice());
  }
}