package com.epam.sax;

import com.epam.model.Medicine;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SAXParserUser {
  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Medicine> parseBeers(File xml, File xsd){
    List<Medicine> beerList = new ArrayList<>();
    try {
      saxParserFactory.setSchema(SAXValidator.createSchema(xsd));

      SAXParser saxParser = saxParserFactory.newSAXParser();
      SAXHandler saxHandler = new SAXHandler();
      saxParser.parse(xml, saxHandler);

      beerList = saxHandler.getMedicineList();
    }catch (SAXException | ParserConfigurationException | IOException ex){
      ex.printStackTrace();
    }

    return beerList;
  }
}
