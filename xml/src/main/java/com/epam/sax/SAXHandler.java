package com.epam.sax;

import com.epam.model.Certificate;
import com.epam.model.Medicine;
import com.epam.model.Package;
import com.epam.model.Versions;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler {

  private List<Medicine> medicineList = new ArrayList<>();
  private Medicine medicine = null;
  private List<String> analogs = new ArrayList<>();
  private Versions versions = new Versions();
  private Certificate certificate = new Certificate();
  private Package aPackage = new Package();
  private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

  private boolean bName = false;
  private boolean bPharm = false;
  private boolean bGroup = false;
  private boolean bAnalog = false;
  private boolean bConsistency = false;
  private boolean bNumber = false;
  private boolean bIssueDate = false;
  private boolean bFinishDate = false;
  private boolean bOrganization = false;
  private boolean bType = false;
  private boolean bCount = false;
  private boolean bPrice = false;
  private boolean bDossage = false;

  public List<Medicine> getMedicineList() {
    return this.medicineList;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    if (qName.equalsIgnoreCase("medicine")) {
      String medicineId = attributes.getValue("medicineId");
      medicine = new Medicine();
      medicine.setMedicineId(Integer.parseInt(medicineId));
    } else if (qName.equalsIgnoreCase("name")) {
      bName = true;
    } else if (qName.equalsIgnoreCase("pharm")) {
      bPharm = true;
    } else if (qName.equalsIgnoreCase("group")) {
      bGroup = true;
    } else if (qName.equalsIgnoreCase("analog")) {
      bAnalog = true;
    } else if (qName.equalsIgnoreCase("consistency")) {
      bConsistency = true;
    } else if (qName.equalsIgnoreCase("number")) {
      bNumber = true;
    } else if (qName.equalsIgnoreCase("issueDate")) {
      bIssueDate = true;
    } else if (qName.equalsIgnoreCase("finishDate")) {
      bFinishDate = true;
    } else if (qName.equalsIgnoreCase("organization")) {
      bOrganization = true;
    } else if (qName.equalsIgnoreCase("type")) {
      bType = true;
    } else if (qName.equalsIgnoreCase("count")) {
      bCount = true;
    } else if (qName.equalsIgnoreCase("price")) {
      bPrice = true;
    } else if (qName.equalsIgnoreCase("dossage")) {
      bDossage = true;
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException {
    if (qName.equalsIgnoreCase("medicine")) {
      medicineList.add(medicine);
    }
  }

  public void characters(char ch[], int start, int length) throws SAXException {
    if (bName) {
      medicine.setName(new String(ch, start, length));
      bName = false;
    } else if (bPharm) {
      medicine.setPharm(new String(ch, start, length));
      bPharm = false;
    } else if (bGroup) {
      medicine.setGroup(new String(ch, start, length));
      bGroup = false;
    } else if (bAnalog) {
      analogs.add(new String(ch, start, length));
      bAnalog = false;
    } else if (bConsistency) {
      versions.setConsistency(new String(ch, start, length));
      bConsistency = false;
    } else if (bNumber) {
      certificate.setNumber(Integer.parseInt(new String(ch, start, length)));
      bNumber = false;
    } else if (bIssueDate) {
      try {
        certificate.setIssueDate(formatter.parse(new String(ch, start, length)));
        bIssueDate = false;
      } catch (ParseException e) {
        System.out.println("Wrong parsing");
      }
      bIssueDate = false;
    } else if (bFinishDate) {
      try {
        certificate.setFinishDate(formatter.parse(new String(ch, start, length)));
        bFinishDate = false;
      } catch (ParseException e) {
        System.out.println("Wrong parsing");
      }
      bFinishDate = false;
    } else if (bOrganization) {
      certificate.setOrganization(new String(ch, start, length));
      versions.setCertificate(certificate);
      bOrganization = false;
    } else if (bType) {
      aPackage.setType(new String(ch, start, length));
      bType = false;
    } else if (bCount) {
      aPackage.setCount(Integer.parseInt(new String(ch, start, length)));
      bCount = false;
    } else if (bPrice) {
      aPackage.setPrice(Integer.parseInt(new String(ch, start, length)));
      versions.setaPackage(aPackage);
      bPrice = false;
    } else if (bDossage) {
      versions.setDossage(new String(ch, start, length));
      medicine.setAnalogs(analogs);
      medicine.setVersions(versions);
      bDossage = false;
      analogs = new ArrayList<>();
      versions = new Versions();
      certificate = new Certificate();
      aPackage = new Package();
    }
  }
}


