<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
        <div style="background-color: green; color: black;">
          <h2>Medicines</h2>
        </div>
        <table border="3">
          <tr bgcolor="#2E9AFE">
            <th>Name</th>
            <th>Pharm</th>
            <th>Group</th>
            <th>Analogs</th>
            <th>Consistency</th>
            <th>Certificate</th>
            <th>Package</th>
            <th>Dosage</th>
          </tr>

          <xsl:for-each select="medicines/medicine">

            <tr>
              <td><xsl:value-of select="name"/></td>
              <td><xsl:value-of select="pharm"/></td>
              <td><xsl:value-of select="group"/></td>
              <td>

                <xsl:for-each select="analogs/analog">
                <xsl:value-of select="."/><br/>
                </xsl:for-each>
              </td>
              <td><xsl:value-of select="versions/consistency"/></td>
              <td>
                <xsl:value-of select="versions/certificate/number"/><br/>
                <xsl:value-of select="versions/certificate/issueDate"/><br/>
                <xsl:value-of select="versions/certificate/finishDate"/><br/>
                <xsl:value-of select="versions/certificate/Organization"/><br/>
              </td>
              <td>
                <xsl:value-of select="versions/package/type"/><br/>
                <xsl:value-of select="versions/package/count"/><br/>
                <xsl:value-of select="versions/package/price"/><br/>
              </td>
              <td><xsl:value-of select="versions/dossage"/></td>
            </tr>

          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
