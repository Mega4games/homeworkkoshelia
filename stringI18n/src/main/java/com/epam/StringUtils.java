package com.epam;

import java.util.ArrayList;

public class StringUtils {

  ArrayList<Object> strings = new ArrayList<Object>();

  public StringUtils addTo(Object obj){
    strings.add(obj);
    return this;
  }

  public String getStrings() {
    StringBuilder str = new StringBuilder();
    for (Object o : strings) {
      str.append(o.toString() + " ");
    }
    return str.toString();
  }
}
