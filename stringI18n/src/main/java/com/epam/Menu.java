package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Menu {

  private Map<String, String> menu;
  private ResourceBundle bundle;
  private Locale locale;

  private void translateMenu() {
    menu = new LinkedHashMap<String, String>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
  }

  public Menu() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    translateMenu();
  }

  private void translateInto(String local) throws IOException {
    locale = new Locale(local);
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    translateMenu();
    show();
  }

  public void show() throws IOException {
    System.out.println("\nMENU:");
    for (String key : menu.keySet()) {
      if (key.length() == 1) {
        System.out.println(menu.get(key));
      }
    }
    choose();
  }

  private void choose() throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Enter your choise: ");
    int choise = Integer.parseInt(reader.readLine());
    switch (choise) {
      case 1: {
        testStringUtil();
        show();
        break;
      }
      case 2: {
        translateInto("uk");
        break;
      }
      case 3: {
        translateInto("en");
        break;
      }
      case 4: {
        translateInto("ru");
        break;
      }
      case 5: {
        testRegExp();
        show();
        break;
      }
      case 6: {
        System.exit(0);
      }
    }
  }

  private void testStringUtil() {
    StringUtils util = new StringUtils();
    util.addTo(45)
        .addTo(" years")
        .addTo(" old.");
    System.out.println(util.getStrings());
  }

  private void testRegExp() {
    String test = "This is the tEst.";
    String pattern = "^[A-Z].*\\.$";
    Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
    Matcher matcher = p.matcher(test);
    System.out.println("Test pattern:");
    System.out.println(matcher.matches());
    String[] strings = test.split("the | you");
    System.out.println("Test split: ");
    for (int i = 0; i < strings.length; i++) {
      System.out.println(strings[i]);
    }
    p = Pattern.compile("[aeiouy]", Pattern.CASE_INSENSITIVE);
    matcher = p.matcher(test);
    System.out.println("Test replace: ");
    System.out.println(matcher.replaceAll("_"));
  }

}
