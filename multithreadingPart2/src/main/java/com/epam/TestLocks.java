package com.epam;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TestLocks {

  private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

  //private Lock lock = readWriteLock.writeLock();

  private CustomLock lock = new CustomLock();

  private CustomLock lock1 = new CustomLock();

  private CustomLock lock2 = new CustomLock();

  private StringBuilder builder = new StringBuilder();

  public void task1() {
    try {
      lock.lock();
      builder.append("This is task1. ");
      System.out.println(builder.toString());
      builder.delete(0, builder.length());
      ReentrantLock lock = new ReentrantLock();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    finally {
      lock.unlock();
    }
  }

  public void task2() {
    try {
      //lock1.lock();
      lock.lock();
      builder.append("This is task2. ");
      System.out.println(builder.toString());
      builder.delete(0, builder.length());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    finally {
      lock.unlock();
      //lock1.unlock();
    }
  }

  public void task3() {
    try {
      //lock2.lock();
      lock.lock();
      builder.append("This is task3. ");
      System.out.println(builder.toString());
      builder.delete(0, builder.length());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    finally {
      lock.unlock();
      //lock2.unlock();
    }
  }

}
