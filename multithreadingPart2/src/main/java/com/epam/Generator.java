package com.epam;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Generator implements Runnable {

  private final BlockingQueue<String> queue;
  private Random random = new Random();

  public Generator(BlockingQueue<String> queue) {
    this.queue = queue;
  }

  @Override
  public void run() {
    try {
      for (int i = 0; i < 15; i++) {
        queue.put(random.nextInt() + "");
      }
      //Special marker for ending
      queue.put("/");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}

