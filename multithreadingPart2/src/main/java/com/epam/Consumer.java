package com.epam;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class Consumer implements Runnable {

  private final BlockingQueue<String> queue;

  public Consumer(BlockingQueue<String> queue) {
    this.queue = queue;
  }

  @Override
  public void run() {
    try {
      System.out.println("Start " + Thread.currentThread().getName());
      String value = queue.take();
      while (!value.equals("/")) {
        System.out.println(value);
        value = queue.take();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
