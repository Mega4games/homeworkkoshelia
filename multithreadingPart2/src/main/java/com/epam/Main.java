package com.epam;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;


public class Main {

  public static void main(String[] args) {
    //testSynchronization();
    testQueue();
  }

  private static void testSynchronization() {
    ExecutorService service = Executors.newCachedThreadPool();
    TestLocks testLocks = new TestLocks();
    for (int i = 0; i < 10; i++) {
      service.submit(testLocks::task1);
      service.submit(testLocks::task2);
      service.submit(testLocks::task3);
    }
    service.shutdown();
  }

  private static void testQueue() {
    ExecutorService service = Executors.newCachedThreadPool();
    BlockingQueue<String> queue = new LinkedBlockingQueue<>();
    service.submit(new Generator(queue));
    service.submit(new Consumer(queue));
    service.shutdown();

  }

}
