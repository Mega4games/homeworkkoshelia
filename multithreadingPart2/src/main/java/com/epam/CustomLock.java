package com.epam;

public class CustomLock {

  private boolean isCurrentLocked = false;
  private Thread threadLock = null;
  private int lockCount = 0;

  public synchronized void lock() throws InterruptedException{
    while(isCurrentLocked && Thread.currentThread() != threadLock){
      this.wait();
    }
    isCurrentLocked = true;
    threadLock = Thread.currentThread();
    lockCount++;
  }

  public synchronized void unlock(){
    if (!isCurrentLocked || threadLock != Thread.currentThread()) {
      return;
    }
    lockCount--;
    if(lockCount == 0){
      isCurrentLocked = false;
      this.notify();
    }
  }
}
