package com.epam;

import java.util.ArrayList;
import java.util.HashSet;

public class Solution {

  public static void main(String[] args) {
    int[] arr1 = {
        5, 7, 8, 8, 5, 8, 8, 9, 9
    };
    int[] arr2 = {
        5, 8,9
    };
    int[] arr = deleteSeries(arr1);
    for (int i = 0; i < arr.length; i++) {
      System.out.println(arr[i]);
    }
  }

  /*Дано два масиви.
  Сформувати третій масив, що складається з тих елементів, які:
  а) присутні в обох масивах; б) присутні тільки в одному з масивів.*/

  //a)
  public static int[] twoToOneA(int[] array1, int[] array2) {
    HashSet<Integer> set = new HashSet<Integer>();
    for (int i = 0; i < array1.length; i++) {
      for (int j = 0; j < array2.length; j++) {
        if (array1[i] == array2[j]) {
          set.add(array1[i]);
        }
      }
    }
    int[] array3 = new int[set.size()];
    int id = 0;
    for (int i :set) {
      array3[id] = i;
      id++;
    }
    return array3;
  }

  //b)
  public static int[] twoToOneB(int[] array1, int[] array2) {
    HashSet<Integer> set = new HashSet<Integer>();
    boolean nonUnique1 = false;
    boolean nonUnique2 = false;
    for (int i = 0; i < array1.length; i++) {
      for (int j = 0; j < array2.length; j++) {
        if (array1[i] == array2[j]) {
          nonUnique1 = true;
        }
        if (array2[i] == array1[j]) {
          nonUnique2 = true;
        }
      }
      if (!nonUnique1) {
        set.add(array1[i]);
      }
      if (!nonUnique2) {
        set.add(array2[i]);

      }
      nonUnique1 = false;
      nonUnique2 = false;
    }
    int[] array3 = new int[set.size()];
    int id = 0;
    for ( Integer i : set) {
      array3[id] = i;
      id++;
    }

    return array3;
  }

  //B. Видалити в масиві всі числа, які повторюються більше двох разів.
  public static int[] deleteNumbers(int[] arr) {
    int number;
    boolean deleted = false;
    ArrayList<Integer> deletedNumbers = new ArrayList<>();
    for (int i = 0; i < arr.length; i++) {
      number = 0;
      for (int j = 0; j < arr.length; j++) {
        if(i != j && arr[i] == arr[j]) {
          number++;
        }
      }
      if(number > 1) {
        deletedNumbers.add(arr[i]);
      }

    }
    number = 0;
    int[]arr2 = new int[arr.length - deletedNumbers.size()];
    for (int i = 0; i < arr.length ; i++) {
      for (int n : deletedNumbers) {
        if(arr[i]==n) {
          deleted = true;
        }
      }
      if(!deleted) {
        arr2[number] = arr[i];
        number++;
      }
      deleted = false;
    }
    return arr2;
  }

  public static int[] deleteSeries(int[] arr) {
    ArrayList<Integer> result = new ArrayList<>();
    result.add(arr[0]);
    for (int i = 0; i < arr.length - 1; i++) {

      if(arr[i+1] != arr[i]) {
        result.add(arr[i+1]);
      }
      else {

      }
    }
    int[] arr2 = new int[result.size()];
    for (int i = 0; i < result.size(); i++) {
      arr2[i] = result.get(i);
    }
    return arr2;
  }


}
