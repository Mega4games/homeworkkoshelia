package com.epam;

import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Integer> {

  private int n;

  public FibonacciCallable(int n) {
    this.n = n;
  }

  public Integer call() throws Exception {
    int result = 1;
    for (int i = 0; i < n; i++) {
      result += fib(i);
    }
    return result;
  }

  private int fib(int number) {
    if(number == 0 || number == 1) {
      return 1;
    }
    return fib(number - 1) + fib(number - 2);
  }
}
