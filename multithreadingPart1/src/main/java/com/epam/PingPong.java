package com.epam;

public class PingPong {

  public final int MAX = 10;
  public volatile int count = 0;
  private volatile boolean switcher = true;

  public synchronized void ping() {
    try {
      if(switcher) {
        count++;
        System.out.println("Ping");
        switcher = false;
      }
      wait();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public synchronized void pong() {
      if(!switcher) {
        count++;
        System.out.println("Pong");
        switcher = true;
      }
      notifyAll();
  }

}
