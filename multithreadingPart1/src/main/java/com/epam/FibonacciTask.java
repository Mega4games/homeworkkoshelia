package com.epam;

public class FibonacciTask implements Runnable {

  private int n;

  public FibonacciTask(int n) {
    this.n = n;
  }

  public void run() {
    for (int i = 0; i < n ; i++) {
      System.out.println(fib(i));
    }
  }

  private int fib(int number) {
    if(number == 0 || number == 1) {
      return 1;
    }
    return fib(number - 1) + fib(number - 2);
  }


}
