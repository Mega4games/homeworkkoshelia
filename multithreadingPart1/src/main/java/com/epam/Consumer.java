package com.epam;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Consumer extends Thread {

  private DataInputStream inputStream;

  public Consumer(InputStream inputStream) {
    this.inputStream = new DataInputStream(inputStream);
  }

  @Override
  public void run() {
    try {
      while (inputStream.available() > 0) {
        try {
          double x = inputStream.readDouble();
          System.out.println(x);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
