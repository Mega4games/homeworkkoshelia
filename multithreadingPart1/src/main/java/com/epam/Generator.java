package com.epam;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.util.Random;

public class Generator extends Thread {

  private DataOutputStream outputStream;
  private Random random = new Random();

  public Generator(OutputStream outputStream) {
    this.outputStream = new DataOutputStream(outputStream);
  }


  @Override
  public void run() {
    for (int i = 0; i < 10; i++) {
      try {
        double n = random.nextDouble();
        outputStream.writeDouble(n);
        outputStream.flush();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
