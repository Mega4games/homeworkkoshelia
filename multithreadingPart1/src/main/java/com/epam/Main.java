package com.epam;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

  public static void main(String[] args) throws Exception {
    //pingPong();
    //fibonacciTask();
    //callableFibonacci();
    //sleepingTasks();
    //testPipe();
    testSynchronization();
  }

  private static void pingPong() {
    final PingPong pingPong = new PingPong();
    new Thread(() -> {
      while (pingPong.count < pingPong.MAX) {
        pingPong.ping();
      }
    }).start();
    new Thread(() -> {
      while (pingPong.count < pingPong.MAX) {
        pingPong.pong();
      }
    }).start();
  }

  private static void fibonacciTask() {
    //using thread
    for (int i = 0; i < 15; i++) {
      new Thread(new FibonacciTask(15)).start();
    }
    //using cashedThreadPool
    ExecutorService pool = Executors.newCachedThreadPool();
    for (int i = 0; i < 15; i++) {
      pool.execute(new FibonacciTask(i));
    }
    pool.shutdown();

    //using fixedThreadPool
    pool = Executors.newFixedThreadPool(5);
    for (int i = 0; i < 15; i++) {
      pool.execute(new FibonacciTask(i));
    }
    pool.shutdown();

    //using fixedThreadPool
    pool = Executors.newFixedThreadPool(5);
    for (int i = 0; i < 15; i++) {
      pool.execute(new FibonacciTask(i));
    }
    pool.shutdown();

  }

  private static void callableFibonacci() {
    ExecutorService pool = Executors.newCachedThreadPool();
    List<Future<Integer>> futures = new ArrayList<>();

    for (int i = 0; i < 15; i++) {
      futures.add(pool.submit(new FibonacciCallable(i)));
    }

    for (Future<Integer> future : futures) {
      try {
        System.out.println(future.get());
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        pool.shutdown();
      }
    }
  }

  private static void sleepingTasks() {
    ScheduledExecutorService pool = Executors.newScheduledThreadPool(4);
    System.out.println("Enter quantity of tasks: ");
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    for (int i = 0; i < n; i++) {
      System.out.println(i);
      pool.schedule(new SleepingTask(), 1, TimeUnit.SECONDS);
    }
    pool.shutdown();
  }

  private static void testPipe() throws IOException {
    PipedOutputStream pipeOut = new PipedOutputStream();
    PipedInputStream pipeIn = new PipedInputStream();

    pipeIn.connect(pipeOut);

    Generator generator = new Generator(pipeOut);
    Consumer consumer = new Consumer(pipeIn);

    generator.start();
    consumer.start();

    PipedReader pipedReader = new PipedReader();
    PipedWriter pipedWriter = new PipedWriter();

    pipedWriter.connect(pipedReader);

    PipedReaderThread readerThread = new PipedReaderThread(pipedReader);

    PipedWriterThread writerThread = new PipedWriterThread(pipedWriter);

    readerThread.start();
    writerThread.start();

  }

  private static void testSynchronization() {
    ExecutorService service = Executors.newCachedThreadPool();
    StringBuilder builder = new StringBuilder();
      service.submit(() -> new TestSynchronization(builder).task1());
      service.submit(() -> new TestSynchronization(builder).task2());
      service.submit(() -> new TestSynchronization(builder).task3());
      service.shutdown();
  }

}
