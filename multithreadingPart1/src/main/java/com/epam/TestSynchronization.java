package com.epam;

public class TestSynchronization {

  private StringBuilder builder;

  public TestSynchronization(StringBuilder builder) {
   this.builder = builder;
  }

  public void task1() {
    synchronized (builder) {
    //synchronized (new StringBuilder()) {
      builder.append("This is task1. ");
      System.out.println(builder.toString());
      builder.delete(0, builder.length());
    }
  }

  public void task2() {
    synchronized (builder) {
    //synchronized (new StringBuilder()) {
      builder.append("This is task2. ");
      System.out.println(builder.toString());
      builder.delete(0, builder.length());
    }
  }

  public void task3() {
    synchronized (builder) {
    //synchronized (new StringBuilder()) {
      builder.append("This is task3. ");
      System.out.println(builder.toString());
      builder.delete(0, builder.length());
    }
  }

}
