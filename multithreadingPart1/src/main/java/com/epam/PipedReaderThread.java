package com.epam;

import java.io.IOException;
import java.io.PipedReader;

public class PipedReaderThread extends Thread {

  private PipedReader pipedReader;

  public PipedReaderThread(PipedReader pipedReader) {
    this.pipedReader = pipedReader;
  }

  @Override
  public void run() {
    try {
      while (true) {
        char c = (char) pipedReader.read();
        if(c != -1) {
          System.out.print(c);
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

