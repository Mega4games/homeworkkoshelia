package com.epam;

import java.util.Random;

public class SleepingTask implements Runnable {

  @Override
  public void run() {
    try {
      long sleepingTime = (new Random().nextInt(10) + 1) * 1000;
      Thread.sleep(sleepingTime);
      System.out.println(
          Thread.currentThread().getName() + " has slept for " + sleepingTime / 1000 + " seconds.");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }
}
