package com.epam;

import java.io.PipedWriter;

public class PipedWriterThread extends Thread {

  private PipedWriter pipedWriter;


  public PipedWriterThread(PipedWriter pipedWriter) {
    this.pipedWriter = pipedWriter;
  }

  @Override
  public void run() {
    try {
      while (true) {
        pipedWriter.write("Test pipes between Threads.\n");
        pipedWriter.flush();
        Thread.sleep(2000);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
