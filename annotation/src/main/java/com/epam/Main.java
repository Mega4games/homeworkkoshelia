package com.epam;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {

  public static void main(String[] args) throws Exception {
    invokeMethods();
  }

  private static void invokeMethods() throws Exception {
    Class<TestAnnotation> clazz = TestAnnotation.class;
    Method method = clazz.getMethod("testAnnotations");
    TestAnnotation testAnnotation = new TestAnnotation();
    method.invoke(testAnnotation);
    Method method1 = clazz.getMethod("method1", int.class, int.class);
    System.out.println(method1.invoke(testAnnotation, 2, 5));
    Method method2 = clazz.getMethod("method2", String.class);
    System.out.println(method2.invoke(testAnnotation, "java."));
    Method method3 = clazz.getMethod("method3", boolean.class);
    System.out.println(method3.invoke(testAnnotation, false));
    Field field = testAnnotation.getClass().getDeclaredField("string");
    field.setAccessible(true);
    field.set(testAnnotation, "qwerty");
    System.out.println(field.get(testAnnotation));
    Method myMethod = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
    myMethod.invoke(testAnnotation, "c++ ", new int[]{5, 0});
    Method myMethod2 = clazz.getDeclaredMethod("myMethod", String[].class);
    myMethod2.invoke(testAnnotation,
        (Object) new String[]{"Programming languages:", "C++", "Java"});
    AnotherTest<TestAnnotation> test = new AnotherTest<>();
    test.showInfo(testAnnotation);
  }


}
