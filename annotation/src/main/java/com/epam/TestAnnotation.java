package com.epam;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Stream;

public class TestAnnotation {

  @Info(name = "String", function = "contain text")
  private String string = "";

  @Info(name = "int", function = "contain number")
  private int number = 0;

  private boolean condition = true;

  public void testAnnotations() {
    Class<TestAnnotation> obj = TestAnnotation.class;
    Field[] fields = obj.getDeclaredFields();
    Stream.of(fields)
        .forEach(field -> {
          if (field.isAnnotationPresent(Info.class)) {
            System.out.println(field);
          }
        });
    System.out.println(fields[0].getAnnotation(Info.class).name());

  }

  public int method1(int a, int b) {
    return a + b;
  }

  public String method2(String str) {
    return str.toUpperCase();
  }

  public boolean method3(boolean value) {
    return !value;
  }

  public void myMethod(String a, int... args) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(a);
    Arrays.stream(args)
        .forEach(stringBuilder::append);
    System.out.println(stringBuilder.toString());
  }

  public void myMethod(String... args) {
    Arrays.stream(args)
        .forEach(System.out::println);
  }

}
