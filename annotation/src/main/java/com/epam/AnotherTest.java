package com.epam;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class AnotherTest<T> {

  public void showInfo(T t) {
    Class<?> clazz = t.getClass();
    System.out.println("Name and simple name: " + clazz.getName() + " " + clazz.getSimpleName());
    System.out.println("Superclass" + clazz.getSuperclass().getName());
    System.out.println("Package" + clazz.getPackage());
    Constructor[] constructors = clazz.getConstructors();
    System.out.println("Constructors:");
    Arrays.stream(constructors)
        .forEach(System.out::println);
    Method[] methods = clazz.getMethods();
    System.out.println("Methods:");
    Arrays.stream(methods)
        .forEach(System.out::println);
    System.out.println("Fields:");
    Field[] fields = clazz.getFields();
    Arrays.stream(fields)
        .forEach(System.out::println);

  }

}
