package com.epam;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    CustomPriorityQueue<String> queue = new CustomPriorityQueue<String>(10,
        new Comparator<String>() {
          public int compare(String o1, String o2) {
            return o1.compareTo(o2);
          }
        });
    queue.add("Vova");
    queue.add("Max");
    queue.add("Ivan");
    System.out.println(queue.element());
    queue.remove();
    System.out.println(queue.element());
    List list = new ArrayList<Integer>();
    list.add("tt");
    System.out.println(list.get(0));
  }

}
