package com.epam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CustomPriorityQueue<T> {
  private int initialCapacity;
  private List<T> list = new ArrayList<T>();
  private Comparator<T> comparator;

  public CustomPriorityQueue(int initialCapacity, Comparator<T> comparator) {
    this.initialCapacity = initialCapacity;
    this.comparator = comparator;
  }

  public void add(T t) {
    list.add(t);
    Collections.sort(list, comparator);
  }

  public T element() {
    return list.get(list.size()-1);
  }
  public void remove() {
    list.remove(list.get(list.size()-1));
    Collections.sort(list, comparator);
  }

  public int size() {
    return list.size();
  }

}
