package com.epam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {

  private static HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
  private static ArrayList<Integer> positions = new ArrayList<Integer>();
  public static void main(String[] args) {
    int[] arr = new int[] {
        2, 44,44,44,44, 4, 33, 33,33,33, 33,33, 4, 6,6,6,6,6,5
    };
    longestPlateau(arr);
    int max = 0;
    int startPosition = 0;
    for (Map.Entry<Integer, Integer> pair : map.entrySet()) {
      if(max < pair.getValue()) {
        startPosition = pair.getKey();
        max = pair.getValue();
      }
    }
    System.out.println("Plateau starts at index " + startPosition + " with length of " + max);
  }


  public static void longestPlateau(int[] array) {
    int position = 0;
    int length = 1;
    for (int i = 0; i < array.length - 2;i++) {
      if(array[i] < array[i+1] && array[i+1] == array[i+2]) {
        position = i+1;
      }
      positions.add(position);
    }
    for (int i = 0; i < positions.size(); i++) {
      int current  = positions.get(i);
      while (array[current] == array[current+1]) {
        length++;
        current = current + 1;
      }
      if(array[current] > array[current + 1]) {
        map.put(positions.get(i), length);
        length = 1;
      }
    }
  }


}
