package com.epam.commands;

@FunctionalInterface
public interface Command {

  public void execute();
}
