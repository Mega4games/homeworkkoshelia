package com.epam.commands;

public class SecondCommand implements Command {

  String argument;

  public SecondCommand(String argument) {
      this.argument = argument;
  }

  @Override
  public void execute() {
    System.out.println(argument.toUpperCase());
  }
}
