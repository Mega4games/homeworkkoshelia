package com.epam.commands;



public class FirstComand implements Command {

  String argument;

  public FirstComand(String argument) {
    this.argument = argument;
  }

  @Override
  public void execute() {
    System.out.println(argument.toLowerCase());
  }
}
