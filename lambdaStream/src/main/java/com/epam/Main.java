package com.epam;

import com.epam.commands.Command;
import com.epam.commands.FirstComand;
import com.epam.commands.FourthCommand;
import com.epam.commands.SecondCommand;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;


public class Main {

  private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

  public static void main(String[] args) throws IOException {
    int choise;

    while (true) {
      System.out.println("1 - " + Menu.FirstTask);
      System.out.println("2 - " + Menu.SecondTask);
      System.out.println("3 - " + Menu.ThirdTask);
      System.out.println("4 - " + Menu.FourthTask);
      System.out.println("5 - " + Menu.Quit);
      System.out.println("Your choice: ");
      choise = Integer.parseInt(reader.readLine());
      switch (choise) {
        case 1:
          firstTask();
          break;
        case 2:
          secondTask();
          break;
        case 3:
          thirdTask();
          break;
        case 4:
          fourthTask();
          break;
        case 5:
          System.exit(0);
        default:
          System.out.println("Wrong choise. Try Again.");
      }
    }

  }

  public static void firstTask() {
    Functional max = (a, b, c) -> (a > b ? (a > c ? a : c) : (b > c ? b : c));
    Functional average = (a, b, c) -> ((a + b + c) / 3);
    System.out.println("Max " + max.lambdaFunc(10, 19, 15));
    System.out.println("Average " + average.lambdaFunc(1, 3, 5));
  }

  public static void secondTask() throws IOException {
    System.out.println("Enter name of command(first/second/third/fourth): ");
    String nameOfCommand = reader.readLine();
    System.out.println("Enter argument: ");
    String argument = reader.readLine();
    switch (nameOfCommand) {
      case "first": {
        FirstComand firstComand = new FirstComand(argument);
        run(() -> firstComand.execute());
        break;
      }
      case "second": {
        SecondCommand secondCommand = new SecondCommand(argument);
        run(secondCommand::execute);
        break;
      }
      case "third": {
        run(new Command() {
          @Override
          public void execute() {
            System.out.println(argument.length());
          }
        });
        break;
      }
      case "fourth": {
        Command fourthCommand = new FourthCommand(argument);
        fourthCommand.execute();
        break;
      }
    }
  }

  public static void run(Command command) {
    command.execute();
  }

  public static void thirdTask() {
    RandomArray randomArray = new RandomArray();
    int[] arr = randomArray.generate3();
    System.out.println("Elements: ");
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i] + " ");
    }
    System.out.println("\nCount: " + Arrays.stream(arr).count());
    System.out.println("Sum: " + Arrays.stream(arr).sum());
    System.out.println("Average: " + Arrays.stream(arr).average());
    System.out.println("Min: " + Arrays.stream(arr).min());
    System.out.println("Max: " + Arrays.stream(arr).max());
    System.out.println("Sum with the help of reduce: " + Arrays.stream(arr)
        .reduce(0, (left, right) -> left + right));
    int average1 = (int) Arrays.stream(arr).average().getAsDouble();
    System.out.println(
        "Count of numbers greater than avg: " + Arrays.stream(arr).filter(n -> (n > average1))
            .count());
  }

  public static void fourthTask() throws IOException {
    String s;
    ArrayList<String> list = new ArrayList<>();
    System.out.println("Enter strings: ");
    while (!(s = reader.readLine()).isEmpty()) {
      list.add(s);
    }

    StringBuilder fullList = new StringBuilder();
    for (String str : list) {
      fullList.append(str + " ");
    }

    long count = 0;

    String[] strings = fullList.toString()
        .replace(",", "")
        .replace(".", "")
        .split(" ");

    count += Arrays.stream(strings)
        .map(String::toLowerCase)
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
        .entrySet().stream()
        .filter(e -> e.getValue() == 1).count();

    System.out.println("Count of unique words " + count);

    System.out.println("Unique words: ");
    Arrays.stream(strings)
        .map(String::toLowerCase)
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
        .entrySet().stream()
        .filter(e -> e.getValue() == 1)
        .forEachOrdered(m -> System.out.println(m.getKey()));

    System.out.println("Occurance number of each word: ");

    Arrays.stream(strings)
        .map(String::toLowerCase)
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
        .entrySet().stream()
        .forEachOrdered(System.out::println);

    String[] characters = fullList.toString().replace(" ", "").split("");

    System.out.println("Occurance number of each char: ");
    Arrays.stream(characters)
        .filter(n -> !(n.matches("^[A-Z]+$")))
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
        .entrySet().stream()
        .forEachOrdered(System.out::println);
  }

}
