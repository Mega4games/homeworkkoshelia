package com.epam;

@FunctionalInterface
public interface Functional {

  int lambdaFunc(int a, int b, int c);

}
