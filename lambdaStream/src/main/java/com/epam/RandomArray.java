package com.epam;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class RandomArray {

  public int[] generate1() {
    return new Random().ints(10, 0, 100).toArray();
  }

  public int[] generate2() {
    return IntStream.generate(() -> ThreadLocalRandom.current().nextInt(100)).limit(10).toArray();
  }

  public int[] generate3() {
    return Arrays.stream(new int[10]).limit(10).map((n) -> new Random().nextInt(100)).toArray();
  }
}
