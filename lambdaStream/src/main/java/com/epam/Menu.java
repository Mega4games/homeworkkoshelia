package com.epam;

public enum Menu {
  FirstTask("First task"),
  SecondTask("Second task"),
  ThirdTask("Third task"),
  FourthTask("Fourth task"),
  Quit("Quit");
  private String description;

  Menu(String description) {
    this.description = description;
  }
}
