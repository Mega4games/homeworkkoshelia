import com.epam.Operations;
import java.lang.reflect.Method;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;


import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class ClassTest {

  @BeforeAll
  public static void beforeClass() {
    System.out.println("Execute before all test methods in the\n"
        + "current class");
  }

  @BeforeEach
  public static void beforeTestMethod() {
    System.out.println("Execute before each test method");
  }

  @AfterAll
  public static void afterClass() {
    System.out.println("Execute after all test methods in the current class");
  }

  @AfterEach
  public static void afterTestMethod() {
    System.out.println("Execute after each test method");
  }

  @Test
  public void testAnswear() {
    Operations operation = new Operations("first operation");
    String ans = operation.getOperation();
    assertTrue(ans.equals("first operation"), "First test has passed.");
  }

  @Test
  public void testMax() {
    Operations max = new Operations("Max of numbers;");
    int maxNumber = max.maxOf2(2, 7);
    assertEquals(maxNumber, 7);
  }

  @Test
  @Ignore
  //@Disabled
  public void testMin() {
    Operations min = new Operations("Min of numbers");
    int minNumber = min.minOf2(11, 0);
    assertEquals(minNumber,0);
  }

  @Test
  public void testAddind() {
    Operations add = new Operations("Add numbers");
    int sum = add.addNumbers(3, 4);
    assertNotEquals(sum, 2);
  }

  @Test
  public void justTest() {
    assumeTrue(false, "Test skipped");
  }

  @Test
  public void testException() {
    Operations infinity = new Operations("Infinity");
    assertThrows(Exception.class, infinity::getInfinity);
  }




}
