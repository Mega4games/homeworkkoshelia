import com.epam.MockOperation;
import com.epam.StringOperation;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class StringOperationTestMockito {

  @Mock
  MockOperation mockOperation;

  @InjectMocks
  StringOperation stringOperation = new StringOperation();

  @Test
  public void testConcat() {
    when(mockOperation.concat("1", "2")).thenReturn("12");
    assertEquals(stringOperation.concating("1", "2"), "12");

  }

}
