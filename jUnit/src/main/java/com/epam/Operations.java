package com.epam;


public class Operations {

  private String nameOfOperation;

  public Operations(String nameOfOperation) {
    this.nameOfOperation = nameOfOperation;
  }

  public String getOperation() {
    return nameOfOperation;
  }

  public int maxOf2(int a, int b) {
    return a > b ? a : b;
  }

  public int minOf2(int a, int b) {
    return 0;
  }

  public int addNumbers(int a, int b) {
    return a + b;
  }

  public void getInfinity() throws Exception {
    throw new Exception("Infinity ");
  }

  private int multiply() {
    return 5 * 10;
  }

}
