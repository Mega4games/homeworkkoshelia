<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Database</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<section>
    <button onclick="window.history.back()" type="button">Back</button>
    <br/><br/>
    <table border="1" cellpadding="8" cellspacing="0">
        <c:forEach items="${tableMetaDataList}" var="tableMeta">
            <jsp:useBean id="tableMeta" scope="page"
                         type="com.koshelia.model.metadata.TableMetaData"/>
            <tr>
                <td> ${tableMeta.toString()}</td>
            </tr>
        </c:forEach>
    </table>
</section>
</body>
</html>