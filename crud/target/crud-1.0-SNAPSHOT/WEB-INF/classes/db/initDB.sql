

DROP TABLE IF EXISTS user_role;
DROP TABLE IF EXISTS meal;
DROP TABLE IF EXISTS user;


CREATE TABLE user
(
  id          INTEGER PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(100)            NOT NULL,
  email       VARCHAR(100)            NOT NULL,
  password    VARCHAR(30)             NOT NULL,
  registered  TIMESTAMP DEFAULT NOW() NOT NULL,
  enabled     BOOL DEFAULT TRUE       NOT NULL,
  calories_per_day  INTEGER DEFAULT 2000 NOT NULL

);

CREATE TABLE user_role
(
  user_id   INTEGER NOT NULL,
  role      VARCHAR(30),
 CONSTRAINT user_roles_idx UNIQUE (user_id, role),
  FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE NO ACTION
);

CREATE TABLE meal
(
  id          INTEGER   PRIMARY KEY AUTO_INCREMENT,
  user_id     INTEGER   NOT NULL,
  date_time   TIMESTAMP NOT NULL,
  description VARCHAR(200)      NOT NULL,
  calories    INT       NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE NO ACTION
);
