<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>
        Login
    </title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <h3>Login</h3>
    <span>Welcome</span>
    <div>
        <header><span>Login</span></header>
        <form action="auth" method="post">
        <input type="text" name="login" placeholder="userName">
        <input type="password" name="password" placeholder="password">
        <button type="submit">Sign In</button>
        </form>
    </div>
</body>

</html>