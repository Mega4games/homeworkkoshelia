DELETE FROM user_role;
DELETE FROM meal;
DELETE FROM user;
ALTER TABLE user AUTO_INCREMENT = 1;
ALTER TABLE meal AUTO_INCREMENT = 1;

INSERT INTO user (name, email, password) VALUES
  ('User', 'user@yandex.ru', 'password'),
  ('Admin', 'admin@gmail.com', 'admin');

INSERT INTO user_role (role, user_id) VALUES
  ('ROLE_USER', 1),
  ('ROLE_ADMIN', 2);

INSERT INTO meal (date_time, description, calories, user_id) VALUES
  ('2015-05-30 10:00:00', 'Завтрак', 500, 1),
  ('2015-05-30 13:00:00', 'Обед', 1000, 1),
  ('2015-05-30 20:00:00', 'Ужин', 500, 2),
  ('2015-05-31 10:00:00', 'Завтрак', 500, 1),
  ('2015-05-31 13:00:00', 'Обед', 1000, 2),
  ('2015-05-31 20:00:00', 'Ужин', 510, 1),
  ('2015-06-01 14:00:00', 'Админ ланч', 510, 1),
  ('2015-06-01 21:00:00', 'Админ ужин', 1500, 2);