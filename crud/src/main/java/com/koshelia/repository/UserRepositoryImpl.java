package com.koshelia.repository;

import com.koshelia.ConnectionManager;
import com.koshelia.Transformer;
import com.koshelia.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class UserRepositoryImpl implements UserRepository {

  private static final String FIND_ALL = "SELECT * FROM user ORDER BY name, email";
  private static final String DELETE = "DELETE FROM user WHERE id=?";
  private static final String CREATE = "INSERT user (id, name, date_time, email, password, registered, enabled, calories_per_day) VALUES (?, ?, ?, ?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE user SET name=?, email=?, password=?, " +
                                "registered=?, enabled=?, calories_per_day=? WHERE id=?";
  private static final String FIND_BY_ID = "SELECT * FROM user WHERE id=?";
  private static final String FIND_BY_EMAIL = "SELECT * FROM user WHERE email=?";


  @Override
  public int create(User user) throws SQLException {
    Connection conn = ConnectionManager.getConnection();
    try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
      ps.setInt(1, user.getId());
      ps.setString(2, user.getName());
      ps.setTimestamp(3, Timestamp.valueOf(user.getRegistered()));
      ps.setString(4, user.getEmail());
      ps.setString(5, user.getPassword());
      ps.setBoolean(6, user.isEnabled());
      ps.setInt(7, user.getCaloriesPerDay());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(User user) throws SQLException {
    Connection conn = ConnectionManager.getConnection();
    try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
      ps.setString(1, user.getName());
      ps.setTimestamp(2, Timestamp.valueOf(user.getRegistered()));
      ps.setString(3, user.getEmail());
      ps.setString(4, user.getPassword());
      ps.setBoolean(5, user.isEnabled());
      ps.setInt(6, user.getCaloriesPerDay());
      ps.setInt(7, user.getId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(int id) throws SQLException {
    Connection conn = ConnectionManager.getConnection();
    try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
      ps.setInt(1, id);
      return ps.executeUpdate();
    }
  }

  @Override
  public User get(int id) throws SQLException {
    User user = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
      ps.setInt(1, id);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          user = (User) new Transformer(User.class).fromResultSetToEntity(resultSet);
          break;
        }
      }
    }
    return user;
  }


  @Override
  public User getByEmail(String email) throws SQLException {
    User user = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_EMAIL)) {
      ps.setString(1, email);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          user = (User) new Transformer(User.class).fromResultSetToEntity(resultSet);
          break;
        }
      }
    }
    return user;
  }

  @Override
  public List<User> getAll() throws SQLException {
    List<User> users = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
        while (resultSet.next()) {
          users.add((User) new Transformer(User.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return users;
  }
}
