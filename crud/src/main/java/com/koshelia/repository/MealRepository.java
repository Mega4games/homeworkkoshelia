package com.koshelia.repository;

import com.koshelia.model.Meal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;


public interface MealRepository {


  int create(Meal meal, int userId) throws SQLException;

  int update(Meal meal, int userId) throws SQLException;


  int delete(int id, int userId) throws SQLException;


  Meal get(int id, int userId) throws SQLException;

  // ORDERED dateTime desc
  List<Meal> getAll(int userId) throws SQLException;

  // ORDERED dateTime desc
  List<Meal> getBetween(LocalDateTime startDate, LocalDateTime endDate, int userId) throws SQLException;

}
