package com.koshelia.repository;

import com.koshelia.ConnectionManager;
import com.koshelia.Transformer;
import com.koshelia.model.Meal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;



public class MealRepositoryImpl implements MealRepository {

  private static final String FIND_ALL = "SELECT * FROM meal WHERE user_id=?";
  private static final String DELETE = "DELETE FROM meal WHERE id=? and user_id=?";
  private static final String CREATE = "INSERT meal (user_id, date_time, description, calories) VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE meal SET date_time=?, description=?, calories=? WHERE id=? and user_id=?";
  private static final String FIND_BY_ID = "SELECT * FROM meal WHERE id=? and user_id=?";
  private static final String GET_BETWEEN = "SELECT * FROM meal WHERE user_id=?  AND date_time BETWEEN  ? AND ? ORDER BY date_time DESC";


  @Override
  public int create(Meal meal, int userId) throws SQLException {
    Connection conn = ConnectionManager.getConnection();
    try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
      ps.setInt(1, userId);
      ps.setTimestamp(2, Timestamp.valueOf(meal.getDateTime()));
      ps.setString(3, meal.getDescription());
      ps.setInt(4, meal.getCalories());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(Meal meal, int userId) throws SQLException {
    Connection conn = ConnectionManager.getConnection();
    try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
      ps.setTimestamp(1, Timestamp.valueOf(meal.getDateTime()));
      ps.setString(2, meal.getDescription());
      ps.setInt(3, meal.getCalories());
      ps.setInt(4, meal.getId());
      ps.setInt(5, userId);
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(int id, int userId) throws SQLException {
    Connection conn = ConnectionManager.getConnection();
    try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
      ps.setInt(1, id);
      ps.setInt(2, userId);
      return ps.executeUpdate();
    }
  }

  @Override
  public Meal get(int id, int userId) throws SQLException {
    Meal meal = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
      ps.setInt(1, id);
      ps.setInt(2, userId);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          meal = (Meal) new Transformer(Meal.class).fromResultSetToEntity(resultSet);
          break;
        }
      }
    }
    return meal;
  }

  @Override
  public List<Meal> getAll(int userId) throws SQLException {

    List<Meal> meals = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
      statement.setInt(1, userId);
      try (ResultSet resultSet = statement.executeQuery()) {
        while (resultSet.next()) {
          Meal meal = (Meal) new Transformer(Meal.class).fromResultSetToEntity(resultSet);
          meals.add(meal);
          //System.out.println(meal);
        }
      }
    }
    return meals;

  }

  @Override
  public List<Meal> getBetween(LocalDateTime startDate, LocalDateTime endDate, int userId) throws SQLException {
    List<Meal> meals = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement statement = connection.prepareStatement(GET_BETWEEN)) {
      statement.setInt(1, userId);
      statement.setTimestamp(2, Timestamp.valueOf(startDate));
      statement.setTimestamp(3, Timestamp.valueOf(endDate));
      try (ResultSet resultSet = statement.executeQuery()) {
        while (resultSet.next()) {
          meals.add((Meal) new Transformer(Meal.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return meals;

  }
}
