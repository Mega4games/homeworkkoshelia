package com.koshelia.repository;


import com.koshelia.model.User;
import java.sql.SQLException;
import java.util.List;

public interface UserRepository {

  int create(User user) throws SQLException;

  int update(User user) throws SQLException;

  int delete(int id) throws SQLException;

  User get(int id) throws SQLException;

  User getByEmail(String email) throws SQLException;

  List<User> getAll() throws SQLException;

}