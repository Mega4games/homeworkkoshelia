package com.koshelia;

import com.koshelia.model.User;

public class AuthorizedUser {
    private static int id = 1;

    public static int id() {
        return id;
    }

    public static void setId(int id) {
        AuthorizedUser.id = id;
    }

    public static int getCaloriesPerDay() {
        return User.DEFAULT_CALORIES_PER_DAY;
    }
}