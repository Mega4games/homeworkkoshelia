package com.koshelia.service;

import com.koshelia.model.Meal;
import com.koshelia.repository.MealRepository;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class MealServiceImpl implements MealService {

  private final MealRepository repository;


  public MealServiceImpl(MealRepository repository) {
    this.repository = repository;
  }

  @Override
  public Meal get(int id, int userId) {
    Meal meal = null;
    try {
      meal =  repository.get(id, userId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return meal;
  }

  @Override
  public void delete(int id, int userId) {
    try {
      repository.delete(id, userId);
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

  @Override
  public List<Meal> getBetweenDateTimes(LocalDateTime startDateTime, LocalDateTime endDateTime,
      int userId) {
    List<Meal> meals = new ArrayList<>();
    try {
      meals = repository.getBetween(startDateTime, endDateTime, userId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return meals;
  }

  @Override
  public List<Meal> getAll(int userId) {
    List<Meal> meals = new ArrayList<>();
    try {
      meals = repository.getAll(userId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return meals;
  }

  @Override
  public int update(Meal meal, int userId) {
    int res = 0;
    try {
      res = repository.update(meal, userId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return res;
  }

  @Override
  public int create(Meal meal, int userId) {
    int res = 0;
    try {
      res = repository.create(meal, userId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return res;
  }

}