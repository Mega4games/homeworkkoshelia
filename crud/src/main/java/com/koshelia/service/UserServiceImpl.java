package com.koshelia.service;

import com.koshelia.model.User;
import com.koshelia.repository.UserRepository;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



public class UserServiceImpl implements UserService {

  private final UserRepository repository;


  public UserServiceImpl(UserRepository repository) {
    this.repository = repository;
  }

  @Override
  public int create(User user) {
    int res = 0;
    try {
      res = repository.create(user);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return res;
  }

  @Override
  public void delete(int id) {
    try {
      repository.delete(id);
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

  @Override
  public User get(int id) {
    User user = null;
    try {
      user = repository.get(id);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return user;
  }

  @Override
  public User getByEmail(String email) {
    User user = null;
    try {
      user = repository.getByEmail(email);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return user;
  }

  @Override
  public List<User> getAll() {
    List<User> users = new ArrayList<>();
    try {
      users = repository.getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return users;
  }

  @Override
  public int update(User user) {
    int res = 0;
    try {
      res = repository.update(user);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return res;
  }
}