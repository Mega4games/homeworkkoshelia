package com.koshelia.service;


import com.koshelia.model.User;
import com.koshelia.util.NotFoundException;
import java.util.List;

public interface UserService {

    int create(User user);

    void delete(int id) throws NotFoundException;

    User get(int id) throws NotFoundException;

    User getByEmail(String email) throws NotFoundException;

    int update(User user);

    List<User> getAll();

}