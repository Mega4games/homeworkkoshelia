package com.koshelia.service;

import com.koshelia.model.metadata.TableMetaData;
import com.koshelia.repository.MetaDataDaoImpl;
import java.sql.SQLException;
import java.util.List;

public class MetaDataService {

    public List<TableMetaData> getTablesStructure() throws SQLException {
        return new MetaDataDaoImpl().getTablesStructure();
    }

}