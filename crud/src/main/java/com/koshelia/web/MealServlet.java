package com.koshelia.web;

import static com.koshelia.util.DateTimeUtil.parseLocalDate;
import static com.koshelia.util.DateTimeUtil.parseLocalTime;

import com.koshelia.model.Meal;
import com.koshelia.repository.MealRepositoryImpl;
import com.koshelia.service.MealServiceImpl;
import com.koshelia.web.meal.MealRestController;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MealServlet extends HttpServlet {

  private MealRestController mealController = new MealRestController(new MealServiceImpl(new MealRepositoryImpl()));

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    String action = request.getParameter("action");
    if (action == null) {
      Meal meal = new Meal(
          LocalDateTime.parse(request.getParameter("dateTime")),
          request.getParameter("description"),
          Integer.parseInt(request.getParameter("calories")));

      if (request.getParameter("id").isEmpty()) {
        mealController.create(meal);
      } else {
        mealController.update(meal, getId(request));
      }
      response.sendRedirect("meals");

    } else if ("filter".equals(action)) {
      LocalDate startDate = parseLocalDate(request.getParameter("startDate"));
      LocalDate endDate = parseLocalDate(request.getParameter("endDate"));
      LocalTime startTime = parseLocalTime(request.getParameter("startTime"));
      LocalTime endTime = parseLocalTime(request.getParameter("endTime"));
      request
          .setAttribute("meals", mealController.getBetween(startDate, startTime, endDate, endTime));
      request.getRequestDispatcher("/meals.jsp").forward(request, response);
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String action = request.getParameter("action");

    switch (action == null ? "all" : action) {
      case "delete":
        int id = getId(request);
        mealController.delete(id);
        response.sendRedirect("meals");
        break;
      case "create":
      case "update":
        final Meal meal = "create".equals(action) ?
            new Meal(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES), "", 1000) :
            mealController.get(getId(request));
        request.setAttribute("meal", meal);
        request.getRequestDispatcher("/mealform.jsp").forward(request, response);
        break;
      case "metadata":
        request.setAttribute("tableMetaDataList", mealController.takeStructureOfDB());
        request.getRequestDispatcher("/metadata.jsp").forward(request, response);
        break;
      case "all":
      default:
        request.setAttribute("meals", mealController.getAll());
        request.getRequestDispatcher("/meals.jsp").forward(request, response);
        break;
    }
  }

  private int getId(HttpServletRequest request) {
    String paramId = Objects.requireNonNull(request.getParameter("id"));
    return Integer.parseInt(paramId);
  }
}