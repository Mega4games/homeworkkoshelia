package com.koshelia.web.meal;

import static com.koshelia.util.Util.orElse;

import com.koshelia.AuthorizedUser;
import com.koshelia.ConnectionManager;
import com.koshelia.model.Meal;
import com.koshelia.model.MealWithExceed;
import com.koshelia.model.metadata.TableMetaData;
import com.koshelia.service.MealService;
import com.koshelia.service.MetaDataService;
import com.koshelia.util.DateTimeUtil;
import com.koshelia.util.MealsUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;




public class MealRestController {

    private final MealService service;

    public MealRestController(MealService service) {
        this.service = service;
    }

    public Meal get(int id) {
        int userId = AuthorizedUser.id();
        return service.get(id, userId);
    }

    public void delete(int id) {
        int userId = AuthorizedUser.id();
        service.delete(id, userId);
    }

    public List<MealWithExceed> getAll() {
        int userId = AuthorizedUser.id();
        return MealsUtil.getWithExceeded(service.getAll(userId), AuthorizedUser.getCaloriesPerDay());
    }

    public int create(Meal meal) {
        int userId = AuthorizedUser.id();
        return service.create(meal, userId);
    }

    public void update(Meal meal, int id) {
        int userId = AuthorizedUser.id();
        meal.setId(id);
        service.update(meal, userId);
    }

    /**
     * <ol>Filter separately
     * <li>by date</li>
     * <li>by time for every date</li>
     * </ol>
     */
    public List<MealWithExceed> getBetween(LocalDate startDate, LocalTime startTime, LocalDate endDate, LocalTime endTime) {
        int userId = AuthorizedUser.id();
        List<Meal> mealsDateFiltered = service.getBetweenDates(
                orElse(startDate, DateTimeUtil.MIN_DATE), orElse(endDate, DateTimeUtil.MAX_DATE), userId);

        return MealsUtil.getFilteredWithExceeded(mealsDateFiltered, AuthorizedUser.getCaloriesPerDay(),
                orElse(startTime, LocalTime.MIN), orElse(endTime, LocalTime.MAX)
        );
    }

    public List<TableMetaData> takeStructureOfDB() {
        try {
            Connection connection = ConnectionManager.getConnection();
            MetaDataService metaDataService = new MetaDataService();
            List<TableMetaData> tables = metaDataService.getTablesStructure();
            System.out.println("TABLE OF DATABASE: "+connection.getCatalog());
            for (TableMetaData table: tables ) {
                System.out.println(table);
            }
            return tables;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}