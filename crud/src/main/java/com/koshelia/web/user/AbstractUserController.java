package com.koshelia.web.user;

import com.koshelia.model.User;
import com.koshelia.service.UserService;
import java.util.List;


public abstract class AbstractUserController {



  private UserService service;

  public List<User> getAll() {

    return service.getAll();
  }

  public User get(int id) {

    return service.get(id);
  }

  public int create(User user) {

    return service.create(user);
  }

  public void delete(int id) {

    service.delete(id);
  }

  public void update(User user, int id) {

    service.update(user);
  }

  public User getByMail(String email) {

    return service.getByEmail(email);
  }
}