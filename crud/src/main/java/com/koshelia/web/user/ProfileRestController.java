package com.koshelia.web.user;

import com.koshelia.AuthorizedUser;
import com.koshelia.model.User;
import com.koshelia.web.user.AbstractUserController;


public class ProfileRestController extends AbstractUserController {

    public User get() {
        return super.get(AuthorizedUser.id());
    }

    public void delete() {
        super.delete(AuthorizedUser.id());
    }

    public void update(User user) {
        super.update(user, AuthorizedUser.id());
    }
}