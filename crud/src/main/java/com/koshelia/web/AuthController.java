package com.koshelia.web;

import com.koshelia.AuthorizedUser;
import com.koshelia.ConnectionManager;
import com.koshelia.model.User;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthController extends HttpServlet {

  private static final String SELECT = "Select * from user where email=?";

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.getRequestDispatcher("/auth.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String login = req.getParameter("login");
    String password = req.getParameter("password");
    User user = new User();
    Connection connection = ConnectionManager.getConnection();
    PreparedStatement preparedStatement = null;
    try {
      preparedStatement = connection.prepareStatement(SELECT);
      preparedStatement.setString(1, login);
      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        user.setId(resultSet.getInt("id"));
        user.setName(resultSet.getString("email"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setRegistered(resultSet.getTimestamp("registered").toLocalDateTime());
        user.setEnabled(resultSet.getBoolean("enabled"));
        user.setCaloriesPerDay(resultSet.getInt("calories_per_day"));
      }
      System.out.println(user);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    if (login.equals(user.getEmail()) && password.equals(user.getPassword())) {
      AuthorizedUser.setId(user.getId());
      resp.sendRedirect("meals");

      //req.getRequestDispatcher("/meals.jsp").forward(req, resp);
    } else {
      req.setAttribute("errorMessage", "Login or password is incorrect");
      req.getRequestDispatcher("/auth.jsp").forward(req, resp);
    }
  }
}
