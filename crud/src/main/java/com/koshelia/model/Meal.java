package com.koshelia.model;


import com.koshelia.model.annotations.Column;
import com.koshelia.model.annotations.PrimaryKey;
import com.koshelia.model.annotations.PrimaryKeyComposite;
import com.koshelia.model.annotations.Table;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Table(name = "meal")
public class Meal {

  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "date_time")
  private LocalDateTime dateTime;
  @Column(name = "description", length = 100)
  private String description;
  @Column(name = "calories")
  private Integer calories;
  @Column(name = "user_id")
  private Integer user_id;

  public Meal(LocalDateTime dateTime, String description, int calories) {
    this.dateTime = dateTime;
    this.description = description;
    this.calories = calories;
  }

  public Meal() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }

  public String getDescription() {
    return description;
  }

  public int getCalories() {
    return calories;
  }

  public LocalDate getDate() {
    return dateTime.toLocalDate();
  }

  public LocalTime getTime() {
    return dateTime.toLocalTime();
  }

  public void setDateTime(LocalDateTime dateTime) {
    this.dateTime = dateTime;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setCalories(int calories) {
    this.calories = calories;
  }

  @Override
  public String toString() {
    return "Meal{" +
        "id=" + id +
        ", dateTime=" + dateTime +
        ", description='" + description + '\'' +
        ", calories=" + calories +
        '}';
  }

  public int getUser_id() {
    return user_id;
  }

  public void setUser_id(int user_id) {
    this.user_id = user_id;
  }
}