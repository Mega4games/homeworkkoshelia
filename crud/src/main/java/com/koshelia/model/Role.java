package com.koshelia.model;

import com.koshelia.model.annotations.Table;

@Table(name = "user_role")
public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}