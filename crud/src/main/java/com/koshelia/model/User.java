package com.koshelia.model;


import com.koshelia.model.annotations.Column;
import com.koshelia.model.annotations.PrimaryKey;
import com.koshelia.model.annotations.PrimaryKeyComposite;
import com.koshelia.model.annotations.Table;
import java.time.LocalDateTime;
import java.util.*;

@Table(name = "user")
public class User {

  public static final int DEFAULT_CALORIES_PER_DAY = 2000;
  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "name", length = 100)
  private String name;
  @Column(name = "email", length = 100)
  private String email;
  @Column(name = "password", length = 30)
  private String password;
  @Column(name = "enabled")
  private boolean enabled = true;
  @Column(name = "registered")
  private LocalDateTime registered = LocalDateTime.now();
  @PrimaryKeyComposite
  @Column(name = "role")
  private Set<Role> roles;

  private int caloriesPerDay = DEFAULT_CALORIES_PER_DAY;

  private List<Meal> meals;

  public User() {
  }

  public User(User u) {
    this(u.getId(), u.getName(), u.getEmail(), u.getPassword(), u.getCaloriesPerDay(),
        u.isEnabled(), u.getRegistered(), u.getRoles());
  }

  public User(Integer id, String name, String email, String password, Role role, Role... roles) {
    this(id, name, email, password, DEFAULT_CALORIES_PER_DAY, true, LocalDateTime.now(),
        EnumSet.of(role, roles));
  }

  public User(Integer id, String name, String email, String password, int caloriesPerDay,
      boolean enabled, LocalDateTime registered, Collection<Role> roles) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.caloriesPerDay = caloriesPerDay;
    this.enabled = enabled;
    this.registered = registered;
    setRoles(roles);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public LocalDateTime getRegistered() {
    return registered;
  }

  public void setRegistered(LocalDateTime registered) {
    this.registered = registered;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public int getCaloriesPerDay() {
    return caloriesPerDay;
  }

  public void setCaloriesPerDay(int caloriesPerDay) {
    this.caloriesPerDay = caloriesPerDay;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public String getPassword() {
    return password;
  }

  public void setRoles(Collection<Role> roles) {
    this.roles = roles.isEmpty() ? Collections.emptySet() : EnumSet.copyOf(roles);
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", email=" + email +
        ", name=" + name +
        ", enabled=" + enabled +
        ", roles=" + roles +
        ", caloriesPerDay=" + caloriesPerDay +
        '}';
  }

  public List<Meal> getMeals() {
    return meals;
  }

  public void setMeals(List<Meal> meals) {
    this.meals = meals;
  }
}