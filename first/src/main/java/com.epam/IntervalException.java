package com.epam;

public class IntervalException extends RuntimeException {

  public IntervalException(String message) {
    super(message);
  }

}
