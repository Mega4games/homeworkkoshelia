package com.epam;

import java.io.IOException;

public class NotNumberException extends Exception {

  public NotNumberException(String message) {
    super(message);
  }

}
