package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {

  private static int fibonacciCount;
  private static int firstNumber;
  private static int lastNumber;

  public static void main(String[] args) {
    testWithResources();
    input();
    printOddEven(firstNumber, lastNumber);
    printFibonacci(lastNumber);
  }

  private static void input() {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      System.out.println("Enter first number:");
      firstNumber = Integer.parseInt(reader.readLine());
      System.out.println("Enter second number:");
      /*while ((lastNumber = Integer.parseInt(reader.readLine())) < firstNumber) {
        System.out.println("Second number must be greater.");
        System.out.println("Enter again:");
      }*/
      if ((lastNumber = Integer.parseInt(reader.readLine())) < firstNumber) {
        throw new IntervalException("Second number must be greater.");
      }
      System.out.println("Enter count of fibonacci numbers: ");
      fibonacciCount = Integer.parseInt(reader.readLine());
      reader.close();
    } catch (NumberFormatException e) {
      try {
        throw new NotNumberException("Not a number. Try again!");
      } catch (NotNumberException exception) {
        System.out.println("Not a number. Try again!");
        input();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void printOddEven(int first, int last) {
    int sumOdd = 0;
    int sumEven = 0;
    System.out.println("Odd numbers: ");
    for (int i = first; i <= last; i++) {
      if (i % 2 != 0) {
        System.out.println(i);
      }
    }
    System.out.println("Even numbers: ");
    for (int i = last; i >= first; i--) {
      if (i % 2 == 0) {
        System.out.println(i);
        sumEven += i;

      }
    }
    System.out.println("Sum of odd numbers: " + sumOdd + " Sum of even numbers: " + sumEven);
  }

  private static void printFibonacci(int last) {
    int oddCount = 1;
    int evenCount = 1;
    int F1, F2;
    if ((last % 2) == 0) {
      F2 = last;
      F1 = last - 1;
    } else {
      F1 = last;
      F2 = last - 1;
    }
    System.out.print("Fibonacci numbers: " + F1 + " " + F2);
    int sumFb;
    for (int i = 2; i < fibonacciCount; i++) {
      sumFb = F1 + F2;
      if (sumFb % 2 == 0) {
        evenCount++;
      } else {
        oddCount++;
      }
      F1 = F2;
      F2 = sumFb;
      System.out.print(" " + sumFb);
    }
    System.out
        .println("\nPersentage of odd numbers: " + (double) oddCount / fibonacciCount * 100 + " %");
    System.out
        .println("Persentage of even numbers: " + (double) evenCount / fibonacciCount * 100 + " %");
  }

  public static void testWithResources() {
    DBConnectionManager dbConnectionManager = new DBConnectionManager();
    try (FakeConnection fakeConnection = dbConnectionManager.getFakeConnection()) {
      System.out.println("Entering the body of try block.");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
