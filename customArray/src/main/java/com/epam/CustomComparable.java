package com.epam;

import java.util.Comparator;

public class CustomComparable implements Comparable<Object> {
  private String str1;
  private String str2;

  public String getStr1() {
    return str1;
  }

  public CustomComparable(String str1, String str2) {
    this.str1 = str1;
    this.str2 = str2;
  }

  @Override
  public int compareTo(Object o) {
    return str1.compareTo(((CustomComparable) o).str1);
  }
  static class CountriesComparator implements Comparator<Object> {

    @Override
    public int compare(Object o1, Object o2) {
      return ((CustomComparable)o1).str1.compareTo(((CustomComparable)o2).str1);
    }
  }
}
