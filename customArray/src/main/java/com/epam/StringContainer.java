package com.epam;

public class StringContainer {
  private final int INIT_SIZE = 16;
  private final int RESIZE = 4;
  private String[] arr = new String[INIT_SIZE];
  private int pointer = 0;

  public void add(String s) {
    if(pointer == arr.length - 1) {
      resize(arr.length * 2);
    }
    arr[pointer++] = s;
  }

  public String get(int index) {
    return arr[index];
  }

  public void resize(int newSize) {
    String[] newArr = new String[newSize];
    System.arraycopy(arr, 0, newArr, 0, pointer);
    arr = newArr;
  }

  public void delete(int index) {
    for (int i = index; i < pointer; i++) {
      arr[i] = arr[i+1];
    }
    arr[pointer] = null;
    if(arr.length > INIT_SIZE && pointer < arr.length / RESIZE) {
      resize(arr.length/2);
    }
  }

  public int size() {
    return pointer;
  }
}
