package com.epam;

import com.epam.CustomComparable.CountriesComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class Main {

  public static void main(String[] args) {
    List<CustomComparable> customList = new ArrayList<>();
    customList.add(new CustomComparable(Locale.GERMANY.getDisplayCountry(), Locale.GERMANY.getDisplayLanguage()));
    customList.add(new CustomComparable(Locale.CANADA.getDisplayCountry(), Locale.CANADA.getDisplayLanguage()));
    customList.add(new CustomComparable(Locale.JAPAN.getDisplayCountry(), Locale.JAPAN.getDisplayLanguage()));

    CustomComparable.CountriesComparator comparator = new CustomComparable.CountriesComparator();
    Collections.sort(customList);
    for (int i = 0; i < customList.size(); i++) {
      System.out.println(customList.get(i).getStr1());
    }
    System.out.println(Collections.binarySearch(customList, Locale.GERMANY.getDisplayCountry(), comparator));
  }

  public static void testContainer() {
    StringContainer strs = new StringContainer();
    ArrayList<String> list = new ArrayList<String>();
    strs.add("Ivan");
    list.add("Ivan");
    strs.add("Vova");
    list.add("Vova");
    strs.add("Max");
    list.add("Max");
    long start = System.nanoTime();
    for (int i = 0; i < strs.size(); i++) {
      System.out.println(strs.get(i));
    }
    System.out.println(System.nanoTime() - start);
    start = System.nanoTime();
    for (int i = 0; i < list.size(); i++) {
      System.out.println(list.get(i));
    }
    System.out.println(System.nanoTime() - start);
  }


}
